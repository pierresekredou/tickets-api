<div align="center">
  <img src="https://nestjs.com/img/logo-small.svg" width="100" alt="Nest Logo"/>
  <img src="https://upload.wikimedia.org/wikipedia/commons/d/d9/Node.js_logo.svg" width="150" alt="Nest Logo"/>
  <img src="https://upload.wikimedia.org/wikipedia/commons/3/32/Mongo-db-logo.png" width="180" alt="Nest Logo"/>
</div>

[circleci-image]: https://img.shields.io/circleci/build/github/nestjs/nest/master?token=abc123def456
[circleci-url]: https://circleci.com/gh/nestjs/nest

  <p align="center">
    This is an API to manage a tickets booking system, designed and implemented following this <a href='https://cci-file-upload-local.s3.eu-west-3.amazonaws.com/general-files/f0b21c6b-7b89-4285-b3b1-611fdffcb556.png'> class diagram </a><br/>
    Main available features include the following:
      <ul>
        <li> Authentication module: Built with advanced packages like Passport and JWT modules, this is a secure and advanced token-based     authentication 
        </li>
        <li> Travels and bookings managements modules: These are the main features for which this system was designed. Here we implemented features like creating, reading, updating, deleting and searching travels, and allow users to book places for their desired travels.
        These modules were built with high-level flexibility and efficiency, following the industry best practices for maximized scalability and performances.
        </li>
        <li> 
          There are other "secondary features" that are implemented to work with the other flows. There are features like email notifications to communicate with users related to their actions in important features like reinitilizing their password, booking places (or tickets) in a travel and cancelling a booking. 
        </li>
      </ul>
  </p>
    <p align="center">
<!-- <a href="https://www.npmjs.com/~nestjscore" target="_blank"><img src="https://img.shields.io/npm/v/@nestjs/core.svg" alt="NPM Version" /></a>
<a href="https://www.npmjs.com/~nestjscore" target="_blank"><img src="https://img.shields.io/npm/l/@nestjs/core.svg" alt="Package License" /></a>
<a href="https://www.npmjs.com/~nestjscore" target="_blank"><img src="https://img.shields.io/npm/dm/@nestjs/common.svg" alt="NPM Downloads" /></a>
<a href="https://circleci.com/gh/nestjs/nest" target="_blank"><img src="https://img.shields.io/circleci/build/github/nestjs/nest/master" alt="CircleCI" /></a> -->
<!-- <a href="https://coveralls.io/github/nestjs/nest?branch=master" target="_blank"><img src="https://coveralls.io/repos/github/nestjs/nest/badge.svg?branch=master#9" alt="Coverage" /></a> -->
<!-- <a href="https://discord.gg/G7Qnnhy" target="_blank"><img src="https://img.shields.io/badge/discord-online-brightgreen.svg" alt="Discord"/></a>
<a href="https://opencollective.com/nest#backer" target="_blank"><img src="https://opencollective.com/nest/backers/badge.svg" alt="Backers on Open Collective" /></a>
<a href="https://opencollective.com/nest#sponsor" target="_blank"><img src="https://opencollective.com/nest/sponsors/badge.svg" alt="Sponsors on Open Collective" /></a>
  <a href="https://paypal.me/kamilmysliwiec" target="_blank"><img src="https://img.shields.io/badge/Donate-PayPal-ff3f59.svg"/></a>
    <a href="https://opencollective.com/nest#sponsor"  target="_blank"><img src="https://img.shields.io/badge/Support%20us-Open%20Collective-41B883.svg" alt="Support us"></a>
  <a href="https://twitter.com/nestframework" target="_blank"><img src="https://img.shields.io/twitter/follow/nestframework.svg?style=social&label=Follow"></a> -->
</p>
  <!--[![Backers on Open Collective](https://opencollective.com/nest/backers/badge.svg)](https://opencollective.com/nest#backer)
  [![Sponsors on Open Collective](https://opencollective.com/nest/sponsors/badge.svg)](https://opencollective.com/nest#sponsor)-->

# REMINDER #
 Don't forget to check your spam folder if you do not see emails from the app after booking creation/cancellation or after having asked to reset your password.

## Description

[Nest](https://github.com/nestjs/nest) framework TypeScript starter repository.

## Installation

```bash
$ npm install
```

## Running the app

```bash
****** CREATE .env FILE BASED ON .env.template *******

# development
$ npm run start

# watch mode
$ npm run start:dev
```

## Test

```bash
# unit tests
$ npm run test

# e2e tests
$ npm run test:e2e

# test coverage
$ npm run test:cov
```
