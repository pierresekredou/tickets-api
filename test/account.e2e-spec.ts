import { Test, TestingModule } from '@nestjs/testing';
import { INestApplication } from '@nestjs/common';
import * as request from 'supertest';
import { AccountController } from '../src/controllers';
import { ChangePasswordDto } from '../src/dto/change-password.dto';
import { AccountService, TokenService } from '../src/services';
import { JwtService } from '@nestjs/jwt';
import { MailModule } from '../src/mail/email.module';
import { AccountRepository } from '../src/repositories';
import { ACCOUNT_MODEL } from '../src/utils/mongoose/mongoose.constant';
import * as dotenv from 'dotenv';
import { getModelToken } from '@nestjs/mongoose';
import { PassportModule } from '../src/passport/passport.module';
import { Connection } from 'mongoose';

describe('AccountController (e2e)', () => {
  let app: INestApplication;
  let accountService: AccountService;

  beforeAll(async () => {
    // let dbConnection: Connection;

    const moduleFixture: TestingModule = await Test.createTestingModule({
      controllers: [AccountController],
      providers: [
        AccountService,
        AccountRepository,
        TokenService,
        JwtService,
        {
          provide: ACCOUNT_MODEL,
          useValue: getModelToken('Account'),
        },
        {
          provide: 'DatabaseConnection',
          useValue: 'DatabaseConnection',
        },
      ],
      imports: [MailModule, PassportModule],
    }).compile();

    app = moduleFixture.createNestApplication();
    await app.init();
    // dbConnection = moduleFixture.get<DatabaseService>(DatabaseService).getDBHandle();
    dotenv.config();

    accountService = moduleFixture.get<AccountService>(AccountService);
  });

  afterAll(async () => {
    await app.close();
  });

  describe('GET /accounts/me', () => {
    it('should return the connected account', async () => {
      const account = { id: 1, name: 'John Doe' };
      jest.spyOn(accountService, 'getConnectedAccount').mockResolvedValue(<any>account);

      const res = await request(app.getHttpServer())
        .get('/accounts/me')
        .set('Authorization', 'Bearer token')
        .expect(200);

      expect(res.body).toEqual(account);
    });

    it('should return 401 if the user is not authenticated', async () => {
      const res = await request(app.getHttpServer()).get('/accounts/me').expect(401);

      expect(res.body).toHaveProperty('statusCode', 401);
    });
  });

  describe('POST /accounts/change-password', () => {
    const changePasswordDto: ChangePasswordDto = {
      oldPassword: 'password',
      newPassword: 'newPassword',
    };

    it('should change the account password', async () => {
      const account = { id: 1, name: 'John Doe' };
      jest.spyOn(accountService, 'changePassword').mockResolvedValue(<any>account);

      const res = await request(app.getHttpServer())
        .post('/accounts/change-password')
        .set('Authorization', 'Bearer token')
        .send(changePasswordDto)
        .expect(200);

      expect(res.body).toEqual(account);
    });

    it('should return 401 if the user is not authenticated', async () => {
      const res = await request(app.getHttpServer())
        .post('/accounts/change-password')
        .send(changePasswordDto)
        .expect(401);

      expect(res.body).toHaveProperty('statusCode', 401);
    });
  });
});
