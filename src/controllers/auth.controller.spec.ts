import { Test, TestingModule } from '@nestjs/testing';
import { AuthController } from './auth.controller';
import { AuthService, TokenService } from '../services';
import { JsonView } from '../utils/helpers/json-response';
import { signinDto, signupDto } from '../__mock__/auth-stub';
import { AccountRepository } from '../repositories';
import { ACCOUNT_MODEL } from '../utils/mongoose/mongoose.constant';
import { JwtService } from '@nestjs/jwt';
import { getModelToken } from '@nestjs/mongoose';

describe('AuthController', () => {
  let controller: AuthController;
  let service: AuthService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [AuthController],
      providers: [
        AuthService,
        AccountRepository,
        TokenService,
        JwtService,
        {
          provide: ACCOUNT_MODEL,
          useValue: getModelToken('Account'),
        },
      ],
    }).compile();

    controller = module.get<AuthController>(AuthController);
    service = module.get<AuthService>(AuthService);
  });

  describe('signUp', () => {
    it('should sign up a new user', async () => {
      const user = { userAccount: { id: 1, firstname: 'test', lastname: 'user', email: 'test@example.com' } };
      jest.spyOn(service, 'signUp').mockResolvedValue(<any>user);
      jest.spyOn(JsonView, 'formatResponse').mockReturnValue(<any>user);

      const result = await controller.signUp(signupDto);

      expect(service.signUp).toHaveBeenCalledWith(signupDto);
      expect(JsonView.formatResponse).toHaveBeenCalledWith(user);
      expect(result).toEqual(JsonView.formatResponse(user));
    });
  });

  describe('signIn', () => {
    it('should sign in an existing user', async () => {
      // const signinDto: SigninDto = { email: 'test@example.com', password: 'test-password' };
      const user = {
        userAccount: { id: 1, firstname: 'test', lastname: 'user', email: 'test@example.com' },
        accessToken: 'test-access-token',
      };
      jest.spyOn(service, 'signIn').mockResolvedValue(<any>user);
      jest.spyOn(JsonView, 'formatResponse').mockReturnValue(<any>user);

      const result = await controller.signIn(signinDto);

      expect(service.signIn).toHaveBeenCalledWith(signinDto);
      expect(JsonView.formatResponse).toHaveBeenCalledWith(user);
      expect(result).toEqual(JsonView.formatResponse(user));
    });
  });
});
