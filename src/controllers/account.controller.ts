import { Body, Controller, Get, Post, Put, UseGuards } from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { UpdateAccountDto } from '../dto/account.dto';
import { ChangePasswordDto } from '../dto/change-password.dto';
import { ForgotPasswordDto } from '../dto/forgot-password.dto';
import { ResetPasswordDto } from '../dto/reset-password.dto';
import { MainJwtAuthGuard } from '../passport/guards/main.guard';
import { AccountService } from '../services/account.service';
import { JsonView } from '../utils/helpers/json-response';
import { PassiveAuthGuard } from '../passport/guards/passive.guard';

@ApiTags('Accounts')
@Controller('accounts')
export class AccountController {
  constructor(private readonly accountService: AccountService) {}

  @Get('me')
  @UseGuards(MainJwtAuthGuard)
  public async getPersonalAccountInformations() {
    const account = await this.accountService.getConnectedAccount();
    return JsonView.formatResponse(account);
  }

  @Post('change-password')
  @UseGuards(MainJwtAuthGuard)
  public async changePassword(@Body() changePasswordDto: ChangePasswordDto) {
    const account = await this.accountService.changePassword(changePasswordDto);
    return JsonView.formatResponse(account);
  }

  @Put('me')
  @UseGuards(MainJwtAuthGuard)
  public async updatePersonalAccountInformations(@Body() newAccountData: UpdateAccountDto) {
    const account = await this.accountService.updateConnectedAccount(newAccountData);
    return JsonView.formatResponse(account);
  }

  @Post('forgot-password')
  @UseGuards(PassiveAuthGuard)
  public async sendEmailToResetPassword(@Body() forgotPasswordDto: ForgotPasswordDto) {
    const response = await this.accountService.sendEmailToResetPassword(forgotPasswordDto);
    return JsonView.formatResponse(response);
  }

  @Post('reset-password')
  @UseGuards(PassiveAuthGuard)
  public async resetPassword(@Body() resetPasswordDto: ResetPasswordDto) {
    const account = await this.accountService.resetPassword(resetPasswordDto);
    return JsonView.formatResponse(account);
  }
}
