import { Test, TestingModule } from '@nestjs/testing';
import { AccountController } from './account.controller';
import { UpdateAccountDto } from '../dto/account.dto';
import { ChangePasswordDto } from '../dto/change-password.dto';
import { ForgotPasswordDto } from '../dto/forgot-password.dto';
import { AccountService, TokenService } from '../services';
import { JsonView } from '../utils/helpers/json-response';
import { ResetPasswordDto } from '../dto/reset-password.dto';
import { AccountRepository } from '../repositories';
import { MailModule } from '../mail/email.module';
import { ACCOUNT_MODEL } from '../utils/mongoose/mongoose.constant';
import { JwtService } from '@nestjs/jwt';
import { getModelToken } from '@nestjs/mongoose';

describe('AccountController', () => {
  let controller: AccountController;
  let service: AccountService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [AccountController],
      providers: [
        AccountService,
        AccountRepository,
        TokenService,
        JwtService,
        {
          provide: ACCOUNT_MODEL,
          useValue: getModelToken('Account'),
        },
      ],
      imports: [MailModule],
    }).compile();

    controller = module.get<AccountController>(AccountController);
    service = module.get<AccountService>(AccountService);
  });

  describe('getPersonalAccountInformations', () => {
    it('should return the connected account information', async () => {
      const connectedAccount = { id: '28904067548', name: 'John' };
      jest.spyOn(service, 'getConnectedAccount').mockResolvedValue(<any>connectedAccount);
      jest.spyOn(JsonView, 'formatResponse').mockReturnValue(<any>connectedAccount);

      const result = await controller.getPersonalAccountInformations();

      expect(service.getConnectedAccount).toHaveBeenCalled();
      expect(JsonView.formatResponse).toHaveBeenCalledWith(connectedAccount);
      expect(result).toBe(connectedAccount);
    });
  });

  describe('changePassword', () => {
    it('should return the account with the new password', async () => {
      const changePasswordDto: ChangePasswordDto = { oldPassword: '123', newPassword: '456' };
      const account = { id: 1, name: 'John', password: '456' };
      jest.spyOn(service, 'changePassword').mockResolvedValue(<any>account);
      jest.spyOn(JsonView, 'formatResponse').mockReturnValue(<any>account);

      const result = await controller.changePassword(changePasswordDto);

      expect(service.changePassword).toHaveBeenCalledWith(changePasswordDto);
      expect(JsonView.formatResponse).toHaveBeenCalledWith(account);
      expect(result).toBe(account);
    });
  });

  describe('updatePersonalAccountInformations', () => {
    it('should return the updated connected account information', async () => {
      const newAccountData: Partial<UpdateAccountDto> = {
        firstname: 'Jane',
        lastname: 'Black',
        email: 'jane.black@gmail.com',
      };
      const updatedAccount = { id: 1, name: 'Jane' };
      jest.spyOn(service, 'updateConnectedAccount').mockResolvedValue(<any>updatedAccount);
      jest.spyOn(JsonView, 'formatResponse').mockReturnValue(<any>updatedAccount);

      const result = await controller.updatePersonalAccountInformations(<any>newAccountData);

      expect(service.updateConnectedAccount).toHaveBeenCalledWith(newAccountData);
      expect(JsonView.formatResponse).toHaveBeenCalledWith(updatedAccount);
      expect(result).toBe(updatedAccount);
    });
  });

  describe('sendEmailToResetPassword', () => {
    it('should send email to reset password', async () => {
      const forgotPasswordDto: ForgotPasswordDto = { email: 'test@example.com' };
      const response = 'OK';
      jest.spyOn(service, 'sendEmailToResetPassword').mockResolvedValue(response);

      const result = await controller.sendEmailToResetPassword(forgotPasswordDto);

      expect(service.sendEmailToResetPassword).toHaveBeenCalledWith(forgotPasswordDto);
      expect(JsonView.formatResponse).toHaveBeenCalledWith(response);
      expect(result).toEqual(JsonView.formatResponse(response));
    });
  });

  describe('resetPassword', () => {
    it('should reset password', async () => {
      const resetPasswordDto: ResetPasswordDto = { password: 'new-password' };
      const account = { id: 1, email: 'test@example.com' };
      jest.spyOn(service, 'resetPassword').mockResolvedValue(<any>account);

      const result = await controller.resetPassword(resetPasswordDto);

      expect(service.resetPassword).toHaveBeenCalledWith(resetPasswordDto);
      expect(JsonView.formatResponse).toHaveBeenCalledWith(account);
      expect(result).toEqual(JsonView.formatResponse(account));
    });
  });
});
