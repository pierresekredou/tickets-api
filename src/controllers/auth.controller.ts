import { Controller, Post, Body } from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { SigninDto } from '../dto/signin.dto';
import { SignupDto } from '../dto/signup.dto';
import { AuthService } from '../services/auth.service';
import { JsonView } from '../utils/helpers/json-response';
import { Account } from 'src/utils/mongoose/interfaces/account.interface';

@ApiTags('Authentication')
@Controller()
export class AuthController {
  constructor(private readonly authService: AuthService) {}

  @Post('signup')
  public async signUp(@Body() signupDto: SignupDto) {
    const account: { userAccount: Account } = await this.authService.signUp(signupDto);
    return JsonView.formatResponse(account);
  }

  @Post('signin')
  public async signIn(@Body() signinDto: SigninDto) {
    const user: { userAccount: Partial<Account>; accessToken: string } = await this.authService.signIn(signinDto);
    return JsonView.formatResponse(user);
  }
}
