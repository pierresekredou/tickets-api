import { Body, Controller, Get, Param, Post, Put, UseGuards } from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { BookingDto } from '../dto/booking.dto';
import { MainJwtAuthGuard } from '../passport/guards/main.guard';
import { BookingService } from '../services';
import { JsonView } from '../utils/helpers/json-response';

@ApiTags('Bookings')
@Controller('bookings')
@UseGuards(MainJwtAuthGuard)
export class BookingController {
  constructor(private readonly bookingService: BookingService) {}

  @Get()
  async getBookings() {
    const bookings = await this.bookingService.getMyBookings();
    return JsonView.formatResponse(bookings);
  }

  @Get(':id')
  async getSingleBooking(@Param('id') bookingId: string) {
    const booking = await this.bookingService.getOneBooking(bookingId);
    return JsonView.formatResponse(booking);
  }

  @Post()
  async createBooking(@Body() bookingDto: BookingDto) {
    const booking = await this.bookingService.createBooking(bookingDto);
    return JsonView.formatResponse(booking);
  }

  @Put(':id')
  async updateBooking(@Param('id') bookingId: string, @Body() bookingDto: BookingDto) {
    const booking = await this.bookingService.updateBooking(bookingId, bookingDto);
    return JsonView.formatResponse(booking);
  }

  @Put(':id/cancel')
  async cancelBooking(@Param('id') bookingId: string) {
    const response = await this.bookingService.cancelBooking(bookingId);
    return JsonView.formatResponse(response);
  }
}
