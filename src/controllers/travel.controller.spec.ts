import { Test } from '@nestjs/testing';
import { TravelController } from './travel.controller';
import { TravelService } from '../services';
import { travelStub } from '../__mock__/travel-stub';
import { TravelRepository } from '../repositories';
import { TRAVEL_MODEL } from '../utils/mongoose/mongoose.constant';
import { JsonView } from '../utils/helpers/json-response';
import { getModelToken } from '@nestjs/mongoose';

describe('TravelController', () => {
  let travelController: TravelController;
  let travelService: TravelService;

  beforeEach(async () => {
    const moduleRef = await Test.createTestingModule({
      controllers: [TravelController],
      providers: [
        TravelService,
        TravelRepository,
        {
          provide: TRAVEL_MODEL,
          useValue: getModelToken('Travel'),
        },
      ],
    }).compile();

    travelController = moduleRef.get<TravelController>(TravelController);
    travelService = moduleRef.get<TravelService>(TravelService);
  });

  describe('searchTravels', () => {
    it('should return the search results from travelService', async () => {
      const searchResults = travelStub[0];
      const searchTravelsSpy = jest.spyOn(travelService, 'searchTravels').mockResolvedValue(<any>searchResults);
      jest.spyOn(JsonView, 'formatResponse').mockReturnValue(<any>searchResults);

      const result = await travelController.searchTravels('New York', 'London', new Date().toISOString());

      expect(searchTravelsSpy).toBeCalled();
      expect(result).toEqual(JsonView.formatResponse(searchResults));
    });
  });

  describe('getAllTravels', () => {
    it('should return all available travels from travelService', async () => {
      const travels = travelStub;
      const getAvailableTravelsSpy = jest.spyOn(travelService, 'getAvailableTravels').mockResolvedValue(<any>travels);
      jest.spyOn(JsonView, 'formatResponse').mockReturnValue(<any>travels);

      const result = await travelController.getAllTravels();

      expect(getAvailableTravelsSpy).toBeCalled();
      expect(result).toEqual(JsonView.formatResponse(travels));
    });
  });

  describe('getTravelDetails', () => {
    it('should return the travel details from travelService', async () => {
      const travelDetails = travelStub[0];
      const getOneTravelSpy = jest.spyOn(travelService, 'getOneTravel').mockResolvedValue(<any>travelDetails);
      jest.spyOn(JsonView, 'formatResponse').mockReturnValue(<any>travelDetails);

      const result = await travelController.getTravelDetails('1');

      expect(getOneTravelSpy).toBeCalledWith('1');
      expect(result).toEqual(JsonView.formatResponse(travelDetails));
    });
  });

  describe('createTravel', () => {
    it('should create a new travel and return the created travel from travelService', async () => {
      const newTravel = { departure: 'New York', destination: 'London', dateTime: new Date() };
      const createdTravel = { id: '1', ...newTravel };
      const createTravelSpy = jest.spyOn(travelService, 'createTravel').mockResolvedValue(<any>createdTravel);
      jest.spyOn(JsonView, 'formatResponse').mockReturnValue(<any>createdTravel);

      const result = await travelController.createTravel(<any>newTravel);

      expect(createTravelSpy).toBeCalledWith(newTravel);
      expect(result).toEqual(JsonView.formatResponse(createdTravel));
    });
  });
});
