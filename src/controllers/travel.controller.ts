import { Body, Controller, Delete, Get, Param, Post, Put, Query, UseGuards } from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { TravelDto } from '../dto/travel.dto';
import { MainJwtAuthGuard } from '../passport/guards/main.guard';
import { TravelService } from '../services';
import { JsonView } from '../utils/helpers/json-response';

@ApiTags('Travels')
@Controller('travels')
@UseGuards(MainJwtAuthGuard)
export class TravelController {
  constructor(private readonly travelService: TravelService) {}

  @Get('search')
  async searchTravels(
    @Query('departure') departure: string,
    @Query('destination') destination: string,
    @Query('dateTime') dateTime: string,
  ) {
    const travels = await this.travelService.searchTravels(departure, destination, dateTime);
    return JsonView.formatResponse(travels);
  }

  @Get()
  async getAllTravels() {
    const travels = await this.travelService.getAvailableTravels();
    return JsonView.formatResponse(travels);
  }

  @Get(':id')
  async getTravelDetails(@Param('id') travelId: string) {
    const travel = await this.travelService.getOneTravel(travelId);
    return JsonView.formatResponse(travel);
  }

  @Post()
  async createTravel(@Body() travelDto: TravelDto) {
    const travel = await this.travelService.createTravel(travelDto);
    return JsonView.formatResponse(travel);
  }

  @Put(':id')
  async updateTravel(@Param('id') travelId: string, @Body() travelDto: TravelDto) {
    const travel = await this.travelService.updateTravel(travelId, travelDto);
    return JsonView.formatResponse(travel);
  }

  @Delete(':id')
  async deleteTravel(@Param('id') travelId: string) {
    const response = await this.travelService.deleteTravel(travelId);
    return JsonView.formatResponse(response);
  }
}
