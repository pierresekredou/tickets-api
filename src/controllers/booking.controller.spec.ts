import { Test, TestingModule } from '@nestjs/testing';
import { BookingController } from './booking.controller';
import { BookingDto } from '../dto/booking.dto';
import { BookingService } from '../services';
import { JsonView } from '../utils/helpers/json-response';
import { bookingDto, bookingStub } from '../__mock__/booking-stub';
import { BookingStatus } from '../utils/enums/booking-status.enum';
import { BookingRepository, TravelRepository, AccountRepository } from '../repositories';
import { ACCOUNT_MODEL, BOOKING_MODEL, TRAVEL_MODEL } from '../utils/mongoose/mongoose.constant';
import { MailModule } from '../mail/email.module';
import { getModelToken } from '@nestjs/mongoose';

describe('BookingController', () => {
  let bookingController: BookingController;
  let bookingService: BookingService;

  beforeEach(async () => {
    const moduleRef: TestingModule = await Test.createTestingModule({
      controllers: [BookingController],
      providers: [
        BookingService,
        BookingRepository,
        TravelRepository,
        AccountRepository,
        {
          provide: BOOKING_MODEL,
          useValue: getModelToken('Booking'),
        },
        {
          provide: TRAVEL_MODEL,
          useValue: getModelToken('Travel'),
        },
        {
          provide: ACCOUNT_MODEL,
          useValue: getModelToken('Account'),
        },
      ],
      imports: [MailModule],
    }).compile();

    bookingService = moduleRef.get<BookingService>(BookingService);
    bookingController = moduleRef.get<BookingController>(BookingController);
  });

  describe('getBookings', () => {
    it('should return an array of bookings', async () => {
      const mockBookings = bookingStub;
      jest.spyOn(bookingService, 'getMyBookings').mockResolvedValue(<any>mockBookings);

      const result = await bookingController.getBookings();

      expect(result).toEqual(JsonView.formatResponse(mockBookings));
    });
  });

  describe('getSingleBooking', () => {
    it('should return a single booking', async () => {
      const mockBooking = bookingStub[0];
      jest.spyOn(bookingService, 'getOneBooking').mockResolvedValue(<any>mockBooking);

      const result = await bookingController.getSingleBooking('1');

      expect(result).toEqual(JsonView.formatResponse(mockBooking));
    });
  });

  describe('createBooking', () => {
    it('should create a new booking', async () => {
      const mockBookingDto: BookingDto = bookingDto;
      const mockBooking = { id: '1', name: 'New Booking' };
      jest.spyOn(bookingService, 'createBooking').mockResolvedValue(<any>mockBooking);

      const result = await bookingController.createBooking(mockBookingDto);

      expect(result).toEqual(JsonView.formatResponse(mockBooking));
    });
  });

  describe('updateBooking', () => {
    it('should update an existing booking', async () => {
      const mockBookingDto: BookingDto = bookingDto;
      const mockBooking = { id: '1', name: 'Updated Booking' };
      jest.spyOn(bookingService, 'updateBooking').mockResolvedValue(<any>mockBooking);

      const result = await bookingController.updateBooking('1', mockBookingDto);

      expect(result).toEqual(JsonView.formatResponse(mockBooking));
    });
  });

  describe('cancelBooking', () => {
    it('should cancel an existing booking', async () => {
      const bookingCanceled = { ...bookingDto, status: BookingStatus.CANCELED };
      jest.spyOn(bookingService, 'cancelBooking').mockResolvedValue(<any>bookingCanceled);

      const result = await bookingController.cancelBooking('aaaa4234494f');

      expect(result).toEqual(JsonView.formatResponse(bookingCanceled));
    });
  });
});
