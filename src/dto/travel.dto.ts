import { IsNotEmpty, IsNumber } from 'class-validator';

export class TravelDto {
  @IsNotEmpty()
  departure: string;

  @IsNotEmpty()
  destination: string;

  @IsNotEmpty()
  dateTime: string;

  @IsNotEmpty()
  @IsNumber()
  price: number;

  @IsNotEmpty()
  @IsNumber()
  totalPlaces: number;
}
