import { IsNotEmpty } from 'class-validator';

export class BookingDto {
  @IsNotEmpty()
  travelId: string;

  @IsNotEmpty()
  numberOfPlaces: number;
}
