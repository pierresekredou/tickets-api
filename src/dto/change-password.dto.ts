import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty } from 'class-validator';

export class ChangePasswordDto {
  @ApiProperty()
  @IsNotEmpty({ message: 'The old password is required' })
  oldPassword: string;

  @ApiProperty()
  @IsNotEmpty({ message: 'The new password is required' })
  newPassword: string;
}
