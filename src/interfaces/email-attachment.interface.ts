export interface IEmailAttachment {
  filename: string;
  content?: string;
  path?: string;
}
