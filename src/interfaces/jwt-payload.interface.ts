import { TokenType } from 'src/utils/enums/token-type.enum';

export interface IJwtPayload {
  email?: string;
  exp: number;
  type: TokenType;
}
