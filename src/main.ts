import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { Logger, ValidationPipe } from '@nestjs/common';
import * as compression from 'compression';
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';
import * as fs from 'fs';

async function bootstrap() {
  const logger = new Logger('Bootstrap');
  const app = await NestFactory.create(AppModule);
  app.enableCors();
  app.use(compression());
  app.useGlobalPipes(
    new ValidationPipe({
      transform: true,
      whitelist: true,
    }),
  );

  if (process.env.NODE_ENV !== 'production') {
    const pkg = JSON.parse(fs.readFileSync('package.json', 'utf8'));
    const options = new DocumentBuilder()
      .setTitle('TICKET-API')
      .setDescription('This is the main API for train tickets management project')
      .addTag('TICKET-API')
      .setVersion(pkg.version)
      .build();
    const document = SwaggerModule.createDocument(app, options);
    SwaggerModule.setup('api/docs', app, document);
  }
  await app.listen(process.env.PORT);
  if (process.env.NODE_ENV !== 'production') {
    logger.log(`Project runing on environment: ${process.env.NODE_ENV?.toLocaleUpperCase()}`);
  }
}
bootstrap();
