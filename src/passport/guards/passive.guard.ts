import { ExecutionContext, Injectable } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { REQUEST_META } from '../../utils/constants';

@Injectable()
export class PassiveAuthGuard extends AuthGuard('jwt') {
  canActivate(context: ExecutionContext) {
    const request = context.switchToHttp().getRequest();
    REQUEST_META.HEADERS = request.headers;
    return true;
  }
}
