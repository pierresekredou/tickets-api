import { ExecutionContext, Injectable, UnauthorizedException } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { REQUEST_META } from '../../utils/constants';

@Injectable()
export class MainJwtAuthGuard extends AuthGuard('jwt') {
  canActivate(context: ExecutionContext) {
    const request = context.switchToHttp().getRequest();
    REQUEST_META.HEADERS = request.headers;
    return super.canActivate(context);
  }
  handleRequest(err, user, info) {
    if (err || !user) {
      let message = 'Invalid authentication. Please signin again';
      if (info) {
        switch (info.message) {
          case 'invalid signature':
          case 'jwt malformed':
          case 'invalid token':
          case 'invalid algorithm':
            message = 'Invalid authentication. Please signin again';
            break;
          case 'No auth token':
            message = 'Authentication is required to access this resource';
            break;
          case 'jwt expired':
            message = 'Session expired. Please signin again';
            break;
        }
      }
      throw err || new UnauthorizedException(message);
    }
    REQUEST_META.CONNECTED_USER = user;
    return user;
  }
}
