import { Module } from '@nestjs/common';
import { PassportModule as NestPassportModule } from '@nestjs/passport';
import { JwtModule } from '@nestjs/jwt';
import { MainJwtAuthGuard } from './guards/main.guard';
import { JwtAuthStrategy } from './strategies/jwt.strategy';
import { AccountRepository } from '../repositories';
import { MongooseModule } from '../utils/mongoose/mongoose.module';

import * as dotenv from 'dotenv';
import { PassiveAuthGuard } from './guards/passive.guard';
dotenv.config();

@Module({
  exports: [JwtModule],
  imports: [
    NestPassportModule.register({
      defaultStrategy: 'jwt',
    }),
    JwtModule.register({
      secret: process.env.JWT_SECRET,
    }),
    MongooseModule,
  ],
  providers: [JwtAuthStrategy, MainJwtAuthGuard, PassiveAuthGuard, AccountRepository],
})
export class PassportModule {}
