import { PassportStrategy } from '@nestjs/passport';
import { ExtractJwt, Strategy } from 'passport-jwt';
import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { IJwtPayload } from '../../interfaces/jwt-payload.interface';
import { AccountRepository } from '../../repositories';
import { TokenType } from '../../utils/enums/token-type.enum';

@Injectable()
export class JwtAuthStrategy extends PassportStrategy(Strategy) {
  constructor(private readonly accountUseCase: AccountRepository) {
    super({
      jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
      ignoreExpiration: false,
      secretOrKey: process.env.JWT_SECRET,
    });
  }

  async validate(payload: IJwtPayload) {
    const { email, type } = payload;
    if (!email || type !== TokenType.AUTH) {
      throw new HttpException('Invalid access token!', HttpStatus.UNAUTHORIZED);
    }
    const account = await this.accountUseCase.findByEmail(email);
    if (!account) {
      throw new HttpException('Invalid access token!', HttpStatus.UNAUTHORIZED);
    }
    return account;
  }
}
