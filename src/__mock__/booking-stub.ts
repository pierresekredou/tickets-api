import { BookingDto } from '../dto/booking.dto';

export const bookingDto: BookingDto = {
  travelId: '64129ef7c548841fdfa4b713',
  numberOfPlaces: 835,
};

export const bookingStub = [
  { id: 'aaaaa4526786534', code: '523678949404', travelId: 'eeb3404495', userAccountId: '637jkdkfffkk' },
  { id: 'bbbbb546278934o', code: '521349039406', travelId: 'bvbb34078434', userAccountId: '54748949hhdjj' },
];
