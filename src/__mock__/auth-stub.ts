import { SigninDto } from '../dto/signin.dto';
import { SignupDto } from '../dto/signup.dto';

export const signupDto: SignupDto = {
  firstname: 'Pierre',
  lastname: 'Sekredou',
  phoneNumber: '+2250151280065',
  email: 'mrp01@hotmail.com',
  password: 'password1',
};

export const signinDto: SigninDto = {
  email: 'pierresekredou01@gmail.com',
  password: 'password1',
};

export const signinStub = {
  userAccount: { ...signupDto, id: '5439a0dec548841fdfa4d74d' },
  accessToken:
    'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ0eXBlIjoiQVVUSCIsImVtYWlsIjoicGllcnJlc2VrcmVkb3UwMUBnbWFpbC5jb20iLCJleHAiOjE2NzkwMjQ3MjIuNTI2LCJpYXQiOjE2Nzg5MzgzMjJ9.CvV3AQjVU1qSeUdKr9CGvQnjDhOHc7MD0ZnsQg-Ztfk',
};

export const ConnectedUser = {
  id: '9988a0dec548841fdfa4d70a',
  firstname: 'Pierre',
  lastname: 'Sekredou',
  phoneNumber: '+2250151280065',
  email: 'pierresekredou01@gmail.com',
  password: 'password1',
  createdAt: new Date(),
  updatedAt: new Date(),
};
