import { TravelDto } from '../dto/travel.dto';

export const travelDto: TravelDto = {
  departure: 'Lens',
  destination: 'Hyères',
  dateTime: '2023-03-10T07:30:00.000+00:00',
  price: 10.0,
  totalPlaces: 504,
};

export const travelStub = [
  { id: '1', departure: 'New York', destination: 'London', dateTime: new Date() },
  { id: '2', departure: 'Blabladougou', destination: 'Korodougou', dateTime: new Date() },
];
