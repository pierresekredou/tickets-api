export const accountDto = {
  id: '456',
  firstname: 'test',
  lastname: 'user',
  email: 'test@example.com',
};

export const accountStub = [
  accountDto,
  {
    id: '457',
    email: 'test@test.com',
    firstname: 'John',
    lastname: 'Doe',
  },
];
