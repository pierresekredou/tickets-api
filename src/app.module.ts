import { Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { PassportModule } from './passport/passport.module';
import * as controllers from './controllers';
import * as services from './services';
import * as repositories from './repositories';
import { MailModule } from './mail/email.module';
import { MongooseModule } from './utils/mongoose/mongoose.module';

@Module({
  imports: [
    MongooseModule,
    ConfigModule.forRoot({
      isGlobal: true,
    }),
    PassportModule,
    MailModule,
  ],
  controllers: [...Object.values(controllers)],
  providers: [...Object.values(services), ...Object.values(repositories)],
})
export class AppModule {}
