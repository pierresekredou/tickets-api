import * as mongoose from 'mongoose';
import { DB_PROVIDER } from './mongoose.constant';
import { MongooseConfig } from './mongoose.config';
import * as fs from 'fs';

export const MongooseProviders = [
  {
    provide: DB_PROVIDER,
    useFactory: async () => {
      (mongoose as any).Promise = global.Promise;
      const mongooseConfig: MongooseConfig = new MongooseConfig();
      // mongoose.set('debug', true);
      mongoose.set('debug', function (coll, method, query, doc) {
        const contentToWrite = JSON.stringify(
          `Coll: ${coll}.${method} **** Qry: ${JSON.stringify(query)} **** Doc: ${doc}`,
          null,
          2,
        );
        const stream = fs.createWriteStream('mongooselogs.log', { flags: 'a' });
        // [...Array(10000)].forEach( function (item,index) {
        stream.write(contentToWrite + '\n');
        // });
        stream.end();
      });
      return await mongoose.connect(mongooseConfig.configure(), {});
    },
  },
];
