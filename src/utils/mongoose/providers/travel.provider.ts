import { Connection } from 'mongoose';
import { TRAVEL_MODEL, DB_PROVIDER } from '../../../utils/mongoose/mongoose.constant';
import { TravelSchema } from '../schema/travel.schema';
import { Travel } from '../../../utils/mongoose/interfaces/travel.interface';

export const TravelProvider = [
  {
    provide: TRAVEL_MODEL,
    useFactory: (connection: Connection) => {
      TravelSchema.pre<Travel>('save', async function (next) {
        next();
      });
      return connection.model(TRAVEL_MODEL, TravelSchema);
    },
    inject: [DB_PROVIDER],
  },
];
