import { MongooseProviders } from '../mongoose.provider';
import { AccountProvider } from './account.provider';
import { BookingProvider } from './booking.provider';
import { TravelProvider } from './travel.provider';

export const Providers = [...MongooseProviders, ...AccountProvider, ...TravelProvider, ...BookingProvider];
