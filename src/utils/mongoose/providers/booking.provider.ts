import { Connection } from 'mongoose';
import { DB_PROVIDER, BOOKING_MODEL } from '../../../utils/mongoose/mongoose.constant';
import { BookingSchema } from '../schema/booking.schema';
import { Booking } from '../../../utils/mongoose/interfaces/booking.interface';

export const BookingProvider = [
  {
    provide: BOOKING_MODEL,
    useFactory: (connection: Connection) => {
      BookingSchema.pre<Booking>('save', async function (next) {
        next();
      });
      return connection.model(BOOKING_MODEL, BookingSchema);
    },
    inject: [DB_PROVIDER],
  },
];
