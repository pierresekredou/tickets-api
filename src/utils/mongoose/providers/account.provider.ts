import { Connection } from 'mongoose';
import { DB_PROVIDER, ACCOUNT_MODEL } from '../../../utils/mongoose/mongoose.constant';
import { AccountSchema } from '../schema/account.schema';
import { Account } from '../../../utils/mongoose/interfaces/account.interface';

export const AccountProvider = [
  {
    provide: ACCOUNT_MODEL,
    useFactory: (connection: Connection) => {
      AccountSchema.pre<Account>('save', async function (next) {
        next();
      });
      return connection.model(ACCOUNT_MODEL, AccountSchema);
    },
    inject: [DB_PROVIDER],
  },
];
