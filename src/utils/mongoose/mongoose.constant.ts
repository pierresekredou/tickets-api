export const DB_PROVIDER = 'DATABASE_CONNECTION';

export const ACCOUNT_MODEL = 'ACCOUNT_MODEL';
export const ACCOUNT_COLLECTION = 'Account';
export const TRAVEL_MODEL = 'TRAVEL_MODEL';
export const TRAVEL_COLLECTION = 'Travel';
export const BOOKING_MODEL = 'BOOKING_MODEL';
export const BOOKING_COLLECTION = 'Booking';
