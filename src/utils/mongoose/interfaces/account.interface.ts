import { Document } from 'mongoose';

export class Account extends Document {
  id: string;
  firstname: string;
  lastname: string;
  phoneNumber: string;
  email: string;
  password: string;
  salt: string;
  status: string;
  createdAt: Date;
  updatedAt: Date;
}
