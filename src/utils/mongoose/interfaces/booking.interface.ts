import { Document } from 'mongoose';

export class Booking extends Document {
  id: string;
  code: string;
  userAccountId: string;
  travelId: string;
  numberOfPlaces: number;
  status: string;
  createdAt: Date;
  updatedAt: Date;
}
