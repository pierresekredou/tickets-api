import { Document } from 'mongoose';

export class Travel extends Document {
  id: string;
  departure: string;
  destination: string;
  dateTime: string;
  price: number;
  totalPlaces: number;
  numberOfPlacesLeft: number;
  createdAt: Date;
  updatedAt: Date;
}
