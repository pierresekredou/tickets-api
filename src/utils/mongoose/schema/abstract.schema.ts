import * as mongoose from 'mongoose';

export const AbstractSchema = new mongoose.Schema({
  createdAt: {
    type: Date,
    default: new Date(),
  },
  updatedAt: {
    type: Date,
    default: null,
  },
});
