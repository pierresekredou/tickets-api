import * as extendSchema from 'mongoose-extend-schema';
import { AbstractSchema } from './abstract.schema';
import { AccountStatus } from '../../../utils/enums/account-status.enum';

export const AccountSchema = extendSchema(AbstractSchema, {
  firstname: {
    type: String,
    required: true,
  },
  lastname: {
    type: String,
  },
  phoneNumber: {
    type: String,
  },
  email: {
    type: String,
    required: true,
    unique: true,
  },
  password: {
    type: String,
    required: true,
  },
  salt: {
    type: String,
    required: true,
  },
  status: {
    type: String,
    default: AccountStatus.ENABLED,
  },
}).set('toJSON', {
  transform: (document, returnedObject) => {
    returnedObject.id = returnedObject._id.toString();
    delete returnedObject._id;
    delete returnedObject.__v;
    delete returnedObject.password;
    delete returnedObject.salt;
  },
});
