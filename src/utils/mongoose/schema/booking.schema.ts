import * as extendSchema from 'mongoose-extend-schema';
import { AbstractSchema } from './abstract.schema';
import { BookingStatus } from '../../../utils/enums/booking-status.enum';

export const BookingSchema = extendSchema(AbstractSchema, {
  code: {
    type: String,
  },

  userAccountId: {
    type: String,
  },
  travelId: {
    type: String,
  },
  numberOfPlaces: {
    type: Number,
    default: 1,
  },
  status: {
    type: String,
    default: BookingStatus.WAITING,
  },
}).set('toJSON', {
  transform: (document, returnedObject) => {
    returnedObject.id = returnedObject._id.toString();
    delete returnedObject._id;
    delete returnedObject.__v;
  },
});
