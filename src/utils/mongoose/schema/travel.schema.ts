import * as extendSchema from 'mongoose-extend-schema';
import { AbstractSchema } from './abstract.schema';

export const TravelSchema = extendSchema(AbstractSchema, {
  departure: {
    type: String,
    required: true,
  },
  destination: {
    type: String,
    required: true,
  },
  dateTime: {
    type: Date,
    required: true,
  },
  price: {
    type: Number,
  },
  totalPlaces: {
    type: Number,
    required: true,
  },
  numberOfPlacesLeft: {
    type: Number,
  },
}).set('toJSON', {
  transform: (document, returnedObject) => {
    returnedObject.id = returnedObject._id.toString();
    delete returnedObject._id;
    delete returnedObject.__v;
  },
});
