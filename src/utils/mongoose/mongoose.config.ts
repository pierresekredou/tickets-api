import { Injectable } from '@nestjs/common';
import { Logger } from '@nestjs/common';

@Injectable()
export class MongooseConfig {
  private readonly logger = new Logger(MongooseConfig.name);
  private connectionString: string;
  public configure(): string {
    this.logger.log('Configuring Mongoose Options');
    return (this.connectionString = `mongodb+srv://${process.env.DB_USER}:${process.env.DB_PASSWORD}@${process.env.DB_HOST}/${process.env.DB_NAME}?retryWrites=true&w=majority`);
  }
}
