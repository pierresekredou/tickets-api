export enum BookingStatus {
  WAITING = 'WAITING',
  USED = 'USED',
  CANCELED = 'CANCELED',
}
