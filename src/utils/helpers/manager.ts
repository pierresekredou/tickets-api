import * as fs from 'fs';

export function getCurrentTimestampInSeconds() {
  return Date.now() / 1000;
}

export function getHtmlFromTemplateFile(templatePath: string, data): string {
  const handlebars = require('handlebars');
  const htmlTemplate = fs.readFileSync(templatePath, 'utf8');
  const template = handlebars.compile(htmlTemplate);
  const html = template(data);
  return html;
}
