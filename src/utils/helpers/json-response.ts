import { Meta } from './types';

export class JsonView {
  public static formatResponse(data, meta?: Meta, statusCode = 200, message = 'OK', error?: any) {
    return {
      data: data,
      statusCode: statusCode,
      message: message,
      meta: meta,
      errors: error,
    };
  }
}
