export type Meta = {
  currentPage: number;
  firstPage: number;
  lastPage: number;
  itemsNumber: number;
  total: number;
};

export type RequestMeta = {
  HEADERS;
  CONNECTED_USER: UserObj;
};

export type UserObj = {
  id: string;
  firstname: string;
  lastname: string;
  phoneNumber: string;
  email: string;
  password: string;
  createdAt: Date;
  updatedAt: Date;
};

export type EmailParams = {
  to: string[];
  attachments?: any[];
};
export interface IResponse<T> {
  data: T;
  meta: Meta;
  msg?: string;
}

export type Order = 'ASC' | 'DESC';
