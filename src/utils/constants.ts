import { RequestMeta } from './helpers/types';

export const REQUEST_META: RequestMeta = {
  HEADERS: null,
  CONNECTED_USER: null,
};

export const AUTH_TOKEN_LIFETIME_IN_SECONDS = 86400;
export const RESET_PASSWORD_TOKEN_LIFETIME_IN_SECONDS = 600;
