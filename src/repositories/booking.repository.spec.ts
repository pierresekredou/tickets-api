import { INestApplication } from '@nestjs/common';
import { Test, TestingModule } from '@nestjs/testing';
import { BookingRepository } from './booking.repository';

describe('BookingRepository', () => {
  let app: INestApplication;
  let testingModule: TestingModule;
  let repository: BookingRepository;

  const mockBookingRepository = {};

  beforeEach(async () => {
    testingModule = await Test.createTestingModule({
      providers: [BookingRepository],
    })
      .overrideProvider(BookingRepository)
      .useValue(mockBookingRepository)
      .compile();
    app = testingModule.createNestApplication();
    repository = app.get<BookingRepository>(BookingRepository);
    await app.init();
  });

  it('BookingRepository - should be defined', () => {
    expect(repository).toBeDefined();
  });
});
