import { BadRequestException, Inject } from '@nestjs/common';
import { TRAVEL_MODEL } from '../utils/mongoose/mongoose.constant';
import { Model } from 'mongoose';
import { Travel } from '../utils/mongoose/interfaces/travel.interface';

export class TravelRepository {
  constructor(
    @Inject(TRAVEL_MODEL)
    private travelModel: Model<Travel>,
  ) {}

  async saveTravel(travelData: Partial<Travel>) {
    const createTravel = new this.travelModel({
      ...travelData,
    });
    const travelSaved = await createTravel.save()?.catch((err) => {
      throw new BadRequestException(err.message);
    });
    return travelSaved;
  }

  async findAll() {
    const travel = await this.travelModel.find()?.catch((err) => {
      throw new BadRequestException(err.message);
    });
    return travel;
  }

  async findAvailableTravelsForBooking() {
    const travel = await this.travelModel
      .find({
        $and: [{ dateTime: { $gte: new Date() } }, { numberOfPlacesLeft: { $gt: 0 } }],
      })
      ?.catch((err) => {
        throw new BadRequestException(err.message);
      });
    return travel;
  }

  async findById(travelId: string) {
    const travel = await this.travelModel.findById(travelId)?.catch((err) => {
      throw new BadRequestException(err.message);
    });
    return travel;
  }

  async updateTravel(travelId: string, travelData: Partial<Travel>) {
    const travelUpdated = await this.travelModel.findByIdAndUpdate(
      travelId,
      { ...travelData, updatedAt: Date.now() },
      {
        new: true,
        runValidators: true,
      },
    );
    return travelUpdated;
  }

  async removeTravel(travelId: string) {
    await this.travelModel.deleteOne({ id: travelId })?.catch((err) => {
      throw new BadRequestException(err.message);
    });
    return 'OK';
  }

  async searchTravels(departure: string, destination: string, dateTime: string) {
    console.log('DEPARTURE RegExp Test: ', this.buildSearchRegex(departure));
    const travelsFound = await this.travelModel
      .find({
        $or: [
          { departure: { $regex: this.buildSearchRegex(departure), $options: 'i' } },
          { destination: { $regex: this.buildSearchRegex(destination), $options: 'i' } },
        ],
        $and: [{ dateTime: { $gte: dateTime } }],
      })
      ?.catch((err) => {
        throw new BadRequestException(err.message);
      });
    return travelsFound;
  }

  buildSearchRegex(value: string) {
    return new RegExp(`/.*${value}.*/`);
  }
}
