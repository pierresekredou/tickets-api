import { BadRequestException, Inject } from '@nestjs/common';
import { BOOKING_MODEL } from '../utils/mongoose/mongoose.constant';
import { Model } from 'mongoose';
import { Booking } from '../utils/mongoose/interfaces/booking.interface';

export class BookingRepository {
  constructor(
    @Inject(BOOKING_MODEL)
    private bookingModel: Model<Booking>,
  ) {}

  async saveBooking(bookingData: Partial<Booking>) {
    const createBooking = new this.bookingModel({
      ...bookingData,
    });
    const bookingSaved = await createBooking.save()?.catch((err) => {
      throw new BadRequestException(err.message);
    });
    return bookingSaved;
  }

  async findAll() {
    return await this.bookingModel.find()?.catch((err) => {
      throw new BadRequestException(err.message);
    });
  }

  async findAllBookingsOfUser(userAccountId: string) {
    return await this.bookingModel.find({ userAccountId })?.catch((err) => {
      throw new BadRequestException(err.message);
    });
  }

  async findById(bookingId: string) {
    const booking = await this.bookingModel.findById(bookingId)?.catch((err) => {
      throw new BadRequestException(err.message);
    });
    return booking;
  }

  async findOneBookingOfUser(bookingId: string, userAccountId: string) {
    const booking = await this.bookingModel
      .findOne({
        $and: [{ _id: bookingId }, { userAccountId }],
      })
      ?.catch((err) => {
        throw new BadRequestException(err.message);
      });
    return booking;
  }

  async updateBooking(bookingId: string, bookingData: Partial<Booking>) {
    const bookingUpdated = await this.bookingModel.findByIdAndUpdate(bookingId, {
      ...bookingData,
      updatedAt: Date.now(),
    });
    return bookingUpdated;
  }

  async removeBooking(bookingId: string) {
    await this.bookingModel.deleteOne({ id: bookingId })?.catch((err) => {
      throw new BadRequestException(err.message);
    });
    return 'OK';
  }
}
