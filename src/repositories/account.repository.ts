import { BadRequestException, Inject } from '@nestjs/common';
import { ACCOUNT_MODEL } from '../utils/mongoose/mongoose.constant';
import { Model } from 'mongoose';
import { Account } from '../utils/mongoose/interfaces/account.interface';

export class AccountRepository {
  constructor(
    @Inject(ACCOUNT_MODEL)
    public accountModel: Model<Account>,
  ) {}

  async saveAccount(accountData: Partial<Account>) {
    const accountObj = new this.accountModel({
      ...accountData,
    });
    const accountSaved = await accountObj.save()?.catch((err) => {
      throw new BadRequestException(err.message);
    });
    return accountSaved;
  }

  async findAll() {
    const account = await this.accountModel.find()?.catch((err) => {
      throw new BadRequestException(err.message);
    });
    return account;
  }

  async findById(id: string) {
    const account = await this.accountModel.findById(id)?.catch((err) => {
      throw new BadRequestException(err.message);
    });
    return account;
  }

  async findByEmail(email: string) {
    const account = await this.accountModel.findOne({ email })?.catch((err) => {
      throw new BadRequestException(err.message);
    });
    return account;
  }

  async updateAccount(accountId: string, accountData: Partial<Account>) {
    const accountUpdated = await this.accountModel
      .findByIdAndUpdate(accountId, {
        ...accountData,
        updatedAt: Date.now(),
      })
      ?.catch((err) => {
        throw new BadRequestException(err.message);
      });
    return accountUpdated;
  }
}
