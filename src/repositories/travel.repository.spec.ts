import { INestApplication } from '@nestjs/common';
import { Test, TestingModule } from '@nestjs/testing';
import { TravelRepository } from './travel.repository';

describe('TravelRepository', () => {
  let app: INestApplication;
  let testingModule: TestingModule;
  let repository: TravelRepository;

  const mockTravelRepository = {};

  beforeEach(async () => {
    testingModule = await Test.createTestingModule({
      providers: [TravelRepository],
    })
      .overrideProvider(TravelRepository)
      .useValue(mockTravelRepository)
      .compile();
    app = testingModule.createNestApplication();
    repository = app.get<TravelRepository>(TravelRepository);
    await app.init();
  });

  it('TravelRepository - should be defined', () => {
    expect(repository).toBeDefined();
  });
});
