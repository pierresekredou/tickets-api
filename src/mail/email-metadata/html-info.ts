export const EmailHtmlInfoByKey = {
  RESET_PASSWORD: {
    templatePath: `${__dirname}/templates/reset-password.html`,
    subject: 'Réinitialisation du mot de passe',
  },

  BOOKING_CREATED: {
    templatePath: `${__dirname}/templates/booking-created.html`,
    subject: 'Nouvelle réservation',
  },

  BOOKING_CANCELED: {
    templatePath: `${__dirname}/templates/booking-canceled.html`,
    subject: 'Réservation annulée!',
  },
};
