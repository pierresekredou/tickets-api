import { EmailType } from '../mail/email-metadata/email-types.enum';
import { EmailParams } from '../utils/helpers/types';

export class IEmail {
  type: EmailType;
  params: EmailParams;
  data: any;
}
