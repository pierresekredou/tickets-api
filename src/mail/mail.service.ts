import { MailerService } from '@nestjs-modules/mailer';
import { Injectable } from '@nestjs/common';
import { IEmailAttachment } from '../interfaces/email-attachment.interface';
import { EmailHtmlInfoByKey } from './email-metadata/html-info';
import { getHtmlFromTemplateFile } from '../utils/helpers/manager';
import { IEmail } from './email.interface';

@Injectable()
export class MailService {
  constructor(private mailerService: MailerService) {}

  async run(requestDto: IEmail) {
    const htmlInfos: {
      templatePath: string;
      subject: string;
    } = EmailHtmlInfoByKey[requestDto.type];
    const html = getHtmlFromTemplateFile(htmlInfos.templatePath, requestDto.data);
    this.sendEmail(process.env.MAIL_FROM, requestDto.params.to, htmlInfos.subject, html, requestDto.params.attachments);
  }

  async sendEmail(from: string, to: string[], subject: string, html: string, attachments?: IEmailAttachment[]) {
    this.mailerService
      .sendMail({
        to,
        from,
        subject,
        html,
        attachments,
      })
      ?.catch((e) => console.log('AN ERROR OCCURED WHILE SENDING THE EMAIL: ', e));
  }
}
