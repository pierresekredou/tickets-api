import { BadRequestException, HttpException, HttpStatus } from '@nestjs/common';
import { BookingDto } from '../dto/booking.dto';
import { BookingRepository, TravelRepository, AccountRepository } from '../repositories';
import { REQUEST_META } from '../utils/constants';
import { Account } from '../utils/mongoose/interfaces/account.interface';
import { Booking } from '../utils/mongoose/interfaces/booking.interface';
import { Travel } from '../utils/mongoose/interfaces/travel.interface';
import { BookingService } from './booking.service';
import { accountDto } from '../__mock__/account-stub';
import { TestingModule, Test } from '@nestjs/testing';
import { connectedUserStub } from '../__mock__/connected-user-stub';
import { MailModule } from '../mail/email.module';
import { BOOKING_MODEL, TRAVEL_MODEL, ACCOUNT_MODEL } from '../utils/mongoose/mongoose.constant';
import { bookingStub } from '../__mock__/booking-stub';
import { BookingStatus } from '../utils/enums/booking-status.enum';
import { getModelToken } from '@nestjs/mongoose';

describe('BookingService', () => {
  let bookingService: BookingService;
  let bookingRepository: BookingRepository;
  let travelRepository: TravelRepository;
  let accountRepository: AccountRepository;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        BookingService,
        BookingRepository,
        TravelRepository,
        AccountRepository,
        {
          provide: BOOKING_MODEL,
          useValue: getModelToken('Booking'),
        },
        {
          provide: TRAVEL_MODEL,
          useValue: getModelToken('Travel'),
        },
        {
          provide: ACCOUNT_MODEL,
          useValue: getModelToken('Account'),
        },
      ],
      imports: [MailModule],
    }).compile();

    bookingService = module.get<BookingService>(BookingService);
    travelRepository = module.get<TravelRepository>(TravelRepository);
    accountRepository = module.get<AccountRepository>(AccountRepository);
    bookingRepository = module.get<BookingRepository>(BookingRepository);
    REQUEST_META.CONNECTED_USER = <any>connectedUserStub;
  });

  describe('createBooking', () => {
    it('should create a new booking and update the travel', async () => {
      const bookingDto: BookingDto = { travelId: '123', numberOfPlaces: 2 };
      const relatedTravel: Partial<Travel> = {
        id: '123',
        numberOfPlacesLeft: 4,
        departure: 'Paris',
        destination: 'London',
        dateTime: new Date().toISOString(),
      };
      const userAccount: Partial<Account> = accountDto;
      const bookingData: Partial<Booking> = { ...bookingDto, userAccountId: '456', code: '123456' };

      // jest.spyOn(REQUEST_META, 'CONNECTED_USER', 'get').mockReturnValueOnce(<any>{ id: '456' });
      jest.spyOn(travelRepository, 'findById').mockResolvedValueOnce(<any>relatedTravel);
      jest.spyOn(bookingRepository, 'saveBooking').mockResolvedValueOnce(<any>bookingData);
      jest.spyOn(accountRepository, 'findById').mockResolvedValueOnce(<any>userAccount);
      jest.spyOn(travelRepository, 'updateTravel').mockResolvedValueOnce(undefined);
      jest.spyOn(bookingService, 'sendBookingCreatedEmail').mockResolvedValueOnce(undefined);

      const result = await bookingService.createBooking(bookingDto);

      expect(bookingRepository.saveBooking).toHaveBeenCalled();
      expect(travelRepository.updateTravel).toHaveBeenCalledWith('123', { ...relatedTravel, numberOfPlacesLeft: 2 });
      expect(bookingService.sendBookingCreatedEmail).toHaveBeenCalledWith(bookingData, relatedTravel);
      expect(result).toEqual(bookingData);
    });

    it('should throw an HttpException with a NOT_ACCEPTABLE status code when there are not enough places left for the travel', async () => {
      const bookingDto: BookingDto = { travelId: '123', numberOfPlaces: 5 };
      const relatedTravel: Partial<Travel> = {
        id: '123',
        numberOfPlacesLeft: 2,
        departure: 'Paris',
        destination: 'London',
        dateTime: new Date().toISOString(),
      };

      // jest.spyOn(REQUEST_META, 'CONNECTED_USER', 'get').mockReturnValueOnce(<any>{ id: '456' });
      jest.spyOn(travelRepository, 'findById').mockResolvedValueOnce(<any>relatedTravel);

      await expect(bookingService.createBooking(bookingDto)).rejects.toThrow(
        new HttpException(
          `Not enough places left for this travel. You can book up to ${relatedTravel.numberOfPlacesLeft} places right now`,
          HttpStatus.NOT_ACCEPTABLE,
        ),
      );
    });
  });

  describe('getMyBookings', () => {
    it('should return an array of bookings', async () => {
      // const bookings = [        { id: '1', name: 'Booking 1' },        { id: '2', name: 'Booking 2' },      ];
      jest.spyOn(bookingRepository, 'findAllBookingsOfUser').mockResolvedValue(<any>bookingStub);

      const result = await bookingService.getMyBookings();

      expect(result).toEqual(bookingStub);
      expect(bookingRepository.findAllBookingsOfUser).toHaveBeenCalled();
    });

    it('should throw BadRequestException if an error occurs', async () => {
      jest.spyOn(bookingRepository, 'findAllBookingsOfUser').mockRejectedValue(new Error());

      await expect(bookingService.getMyBookings()).rejects.toThrow(BadRequestException);
      expect(bookingRepository.findAllBookingsOfUser).toHaveBeenCalled();
    });
  });

  describe('getOneBooking', () => {
    it('should return a booking', async () => {
      // const booking = { id: '1', name: 'Booking 1' };
      jest.spyOn(bookingRepository, 'findOneBookingOfUser').mockResolvedValue(<any>bookingStub[0]);

      const result = await bookingService.getOneBooking(bookingStub[0].id);

      expect(result).toEqual(bookingStub[0]);
      expect(bookingRepository.findOneBookingOfUser).toHaveBeenCalledWith(
        bookingStub[0].id,
        REQUEST_META.CONNECTED_USER.id,
      );
    });

    it('should throw BadRequestException if an error occurs', async () => {
      jest.spyOn(bookingRepository, 'findOneBookingOfUser').mockRejectedValue(new Error());

      await expect(bookingService.getOneBooking('1')).rejects.toThrow(BadRequestException);
      expect(bookingRepository.findOneBookingOfUser).toHaveBeenCalledWith('1', REQUEST_META.CONNECTED_USER.id);
    });
  });

  describe('BookingService - updateBooking', () => {
    it('should find and update an existing booking object', async () => {
      // Arrange
      const mockBookingRepository = {
        findOneBookingOfUser: jest
          .fn()
          .mockResolvedValueOnce({ id: '1111', status: BookingStatus.WAITING, numberOfPlaces: 2 }),
        updateBooking: jest.fn(),
      };
      const mockTravelRepository = {
        findById: jest.fn().mockResolvedValueOnce({ id: '1111', numberOfPlacesLeft: 2 }),
        updateTravel: jest.fn(),
      };
      const bookingService = new BookingService(
        <any>mockBookingRepository,
        <any>mockTravelRepository,
        undefined,
        undefined,
      );
      const bookingId = '1111';
      const bookingDto = { travelId: '1111', numberOfPlaces: 3 };

      // Act & Assert
      expect(await bookingService.updateBooking(bookingId, bookingDto)).toBe(undefined);
    });
  });

  describe('BookingService - cancelBooking', () => {
    it('should cancel booking and send cancellation email', async () => {
      // Mock data
      const bookingId = '1';
      const bookingFound = {
        id: bookingId,
        travelId: '3',
        numberOfPlaces: 2,
        status: BookingStatus.WAITING,
      };
      const relatedTravel = {
        id: '3',
        numberOfPlacesLeft: 5,
      };
      const userAccount = {
        id: REQUEST_META.CONNECTED_USER.id,
        firstname: 'John',
        lastname: 'Doe',
        email: 'johndoe@example.com',
      };
      const updatedBooking = {
        ...bookingFound,
        status: BookingStatus.CANCELED,
      };

      // Mock repositories and services
      const bookingRepository = {
        findOneBookingOfUser: jest.fn().mockResolvedValue(bookingFound),
        updateBooking: jest.fn().mockResolvedValue(updatedBooking),
      };
      const travelRepository = {
        findById: jest.fn().mockResolvedValue(relatedTravel),
        updateTravel: jest.fn().mockResolvedValue(relatedTravel),
      };
      const accountRepository = {
        findById: jest.fn().mockResolvedValue(userAccount),
      };
      const mailService = {
        run: jest.fn(),
      };

      // Create instance of BookingService and call cancelBooking
      const bookingService = new BookingService(
        <any>bookingRepository,
        <any>travelRepository,
        <any>accountRepository,
        <any>mailService,
      );
      await bookingService.cancelBooking(bookingId);

      // Assert
      expect(bookingRepository.findOneBookingOfUser).toHaveBeenCalledWith(bookingId, REQUEST_META.CONNECTED_USER.id);
      expect(travelRepository.findById).toHaveBeenCalledWith(relatedTravel.id);
      expect(bookingRepository.updateBooking).toHaveBeenCalledWith(bookingId, {
        status: BookingStatus.CANCELED,
        ...bookingFound,
      });
    });

    it('should throw a 404 error if the booking is not found', async () => {
      // Arrange
      const bookingRepositoryMock = {
        findOneBookingOfUser: jest.fn().mockResolvedValue(undefined),
      };
      const travelRepositoryMock = {
        findById: jest.fn().mockResolvedValue({}),
      };
      const accountRepositoryMock = {
        findById: jest.fn().mockResolvedValue({}),
      };
      const mailServiceMock = {
        run: jest.fn(),
      };
      const bookingService = new BookingService(
        <any>bookingRepositoryMock,
        <any>travelRepositoryMock,
        <any>accountRepositoryMock,
        <any>mailServiceMock,
      );

      // Act + Assert
      await expect(bookingService.cancelBooking('invalid-id')).rejects.toThrow(HttpException);
      await expect(bookingService.cancelBooking('invalid-id')).rejects.toThrow('Specified item was not found!');
    });
  });
});
