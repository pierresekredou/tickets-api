import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { IJwtPayload } from '../interfaces/jwt-payload.interface';
import {
  AUTH_TOKEN_LIFETIME_IN_SECONDS,
  REQUEST_META,
  RESET_PASSWORD_TOKEN_LIFETIME_IN_SECONDS,
} from '../utils/constants';
import { TokenType } from '../utils/enums/token-type.enum';
import { getCurrentTimestampInSeconds } from '../utils/helpers/manager';

@Injectable()
export class TokenService {
  constructor(private readonly jwtService: JwtService) {}

  getToken(type: TokenType, data: { email?: string }) {
    let expirationTime: number;
    switch (type) {
      case TokenType.AUTH:
        expirationTime = AUTH_TOKEN_LIFETIME_IN_SECONDS;
        break;
      case TokenType.RESET_PASSWORD:
        expirationTime = RESET_PASSWORD_TOKEN_LIFETIME_IN_SECONDS;
        break;
    }
    const payload: IJwtPayload = {
      type,
      email: data.email,
      exp: getCurrentTimestampInSeconds() + expirationTime,
    };
    return this.jwtService.sign(payload, { secret: process.env.JWT_SECRET || 'superTopSecretKey' });
  }

  validateToken(validType: TokenType, message?: string, ignoreExpiration?: boolean) {
    !message && (message = 'Token expired. Please restart the process');
    const givenToken = REQUEST_META.HEADERS?.token;
    if (!givenToken) throw new HttpException('Access denied!', HttpStatus.NOT_ACCEPTABLE);
    try {
      <IJwtPayload>this.jwtService.verify(givenToken);
    } catch (e) {
      throw new HttpException('Invalid token!', HttpStatus.UNAUTHORIZED);
    }
    const encodedData = <IJwtPayload>this.jwtService.decode(givenToken);
    if (!encodedData) throw new HttpException('Invalid token', HttpStatus.NOT_ACCEPTABLE);
    const { email, type, exp } = encodedData;
    if (type !== validType) throw new HttpException('Invalid token', HttpStatus.NOT_ACCEPTABLE);
    if (!ignoreExpiration)
      if (+exp < getCurrentTimestampInSeconds()) throw new HttpException(message, HttpStatus.NOT_ACCEPTABLE);
    return { email };
  }
}
