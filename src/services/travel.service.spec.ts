import { INestApplication } from '@nestjs/common';
import { Test, TestingModule } from '@nestjs/testing';
import { TravelService } from './travel.service';
import { TravelRepository } from '../repositories';
import { travelDto } from '../__mock__/travel-stub';

describe('TravelService', () => {
  let app: INestApplication;
  let testingModule: TestingModule;
  let service: TravelService;
  let repository: TravelRepository;

  const mockTravelService = {
    createTravel: jest.fn(),
    getAvailableTravels: jest.fn(),
    getOneTravel: jest.fn(),
    updateTravel: jest.fn(),
    deleteTravel: jest.fn(),
  };
  const mockTravelRepository = {
    saveTravel: jest.fn(),
    findAvailableTravelsForBooking: jest.fn(),
    findById: jest.fn(),
    updateTravel: jest.fn(),
    removeTravel: jest.fn(),
  };

  beforeEach(async () => {
    testingModule = await Test.createTestingModule({
      providers: [
        TravelService,
        TravelRepository,
        {
          provide: TravelService,
          useValue: mockTravelService,
        },
      ],
    })
      .overrideProvider(TravelRepository)
      .useValue(mockTravelRepository)
      .compile();
    app = testingModule.createNestApplication();
    service = app.get<TravelService>(TravelService);
    repository = app.get<TravelRepository>(TravelRepository);
    await app.init();
  });

  it('TravelService - should be defined', () => {
    expect(service).toBeDefined();
  });
  it('TravelRepository - should be defined', () => {
    expect(repository).toBeDefined();
  });

  describe('createTravel', () => {
    it('should call the appropriate repository method with received data', async () => {
      const createTravelSpy = jest.spyOn(service, 'createTravel');
      const saveTravelSpy = jest.spyOn(repository, 'saveTravel');

      expect(await service.createTravel(travelDto)).toEqual(undefined);
      expect(createTravelSpy).toHaveBeenCalledTimes(1);
      expect(createTravelSpy).toHaveBeenCalledWith(travelDto);
      expect(await repository.saveTravel(travelDto)).toEqual(undefined);
      expect(saveTravelSpy).toHaveBeenCalledWith(travelDto);
    });
  });

  describe('getAvailableTravels', () => {
    it('should call the repository method to get available travels data', async () => {
      const getTravelsSpy = jest.spyOn(service, 'getAvailableTravels');
      const findAllSpy = jest.spyOn(repository, 'findAvailableTravelsForBooking');

      expect(await service.getAvailableTravels()).toEqual(undefined);
      expect(getTravelsSpy).toHaveBeenCalledTimes(1);
      expect(await repository.findAvailableTravelsForBooking()).toEqual(undefined);
      expect(findAllSpy).toHaveBeenCalled();
    });
  });

  describe('getOneTravel', () => {
    it('should call the repository method to get travel with specified id', async () => {
      const getOneTravelSpy = jest.spyOn(service, 'getOneTravel');
      const findByIdSpy = jest.spyOn(repository, 'findById');

      const id = '6412a0dec548841fdfa4b71e';

      expect(await service.getOneTravel(id)).toEqual(undefined);
      expect(getOneTravelSpy).toHaveBeenCalledWith(id);
      expect(await repository.findById(id)).toEqual(undefined);
      expect(findByIdSpy).toHaveBeenCalled();
    });
  });

  describe('updateTravel', () => {
    it('should call the appropriate repository method to update travel data', async () => {
      const updateTravelSpy = jest.spyOn(service, 'updateTravel');

      const id = '6412a0dec548841fdfa4b71e';

      expect(await service.updateTravel(id, travelDto)).toEqual(undefined);
      expect(updateTravelSpy).toHaveBeenCalledWith(id, travelDto);
    });
  });

  describe('deleteTravel', () => {
    it('should call the appropriate repository method to remove travel data based on received id', async () => {
      const deleteTravelSpy = jest.spyOn(service, 'deleteTravel');
      const removeTravelSpy = jest.spyOn(repository, 'removeTravel');

      const id = '6412a0dec548841fdfa4b71e';

      expect(await service.deleteTravel(id)).toEqual(undefined);
      expect(deleteTravelSpy).toHaveBeenCalledWith(id);
      expect(await repository.removeTravel(id)).toEqual(undefined);
      expect(removeTravelSpy).toHaveBeenCalledWith(id);
    });
  });
});
