import { Test, TestingModule } from '@nestjs/testing';
import { AccountService } from './account.service';
import { AccountRepository } from '../repositories';
import { MailService } from '../mail/mail.service';
import { TokenService } from './token.service';
import { ForgotPasswordDto } from '../dto/forgot-password.dto';
import { ResetPasswordDto } from '../dto/reset-password.dto';
import { UpdateAccountDto } from '../dto/account.dto';
import { HttpException, HttpStatus } from '@nestjs/common';
import { TokenType } from '../utils/enums/token-type.enum';
import { EmailType } from '../mail/email-metadata/email-types.enum';
import * as bcrypt from 'bcrypt';
import { ACCOUNT_MODEL } from '../utils/mongoose/mongoose.constant';
import { MailModule } from '../mail/email.module';
import { ChangePasswordDto } from '../dto/change-password.dto';
import { JwtService } from '@nestjs/jwt';
import { REQUEST_META } from '../utils/constants';
import { connectedUserStub } from '../__mock__/connected-user-stub';
import { getModelToken } from '@nestjs/mongoose';

describe('AccountService', () => {
  let accountService: AccountService;
  let accountRepository: AccountRepository;
  let mailService: MailService;
  let tokenService: TokenService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        AccountService,
        AccountRepository,
        MailService,
        TokenService,
        JwtService,
        {
          provide: ACCOUNT_MODEL,
          useValue: getModelToken('Account'),
        },
      ],
      imports: [MailModule],
    }).compile();

    accountService = module.get<AccountService>(AccountService);
    accountRepository = module.get<AccountRepository>(AccountRepository);
    mailService = module.get<MailService>(MailService);
    tokenService = module.get<TokenService>(TokenService);
    REQUEST_META.CONNECTED_USER = <any>connectedUserStub;
  });

  describe('updateConnectedAccount', () => {
    const updateAccountDto: Partial<UpdateAccountDto> = {
      firstname: 'Jane',
      lastname: 'Doe',
      email: 'jane.doe@hotmail.com',
    };

    it('should throw HttpException with CONFLICT status code if account was not found', async () => {
      jest.spyOn(accountRepository, 'findById').mockResolvedValueOnce(undefined);

      await expect(accountService.updateConnectedAccount(<any>updateAccountDto)).rejects.toThrow(
        new HttpException('Specified account was not found', HttpStatus.CONFLICT),
      );
    });

    it('should throw HttpException with CONFLICT status code if an account with specified email already exists', async () => {
      const connectedUser = { id: '1' };
      jest.spyOn(accountRepository, 'findById').mockResolvedValueOnce(<any>connectedUser);
      jest.spyOn(accountRepository, 'findByEmail').mockResolvedValueOnce(<any>{ id: '2' });

      await expect(accountService.updateConnectedAccount(<any>updateAccountDto)).rejects.toThrow(
        new HttpException(
          'An account with specified email address already exists. Please choose another one',
          HttpStatus.CONFLICT,
        ),
      );
    });

    it('should return the updated account', async () => {
      const connectedUser = { id: '1', firstname: 'John', lastname: 'Doe' };
      jest.spyOn(accountRepository, 'findById').mockResolvedValueOnce(<any>connectedUser);
      jest.spyOn(accountRepository, 'findByEmail').mockResolvedValueOnce(undefined);
      jest.spyOn(accountRepository, 'updateAccount').mockResolvedValueOnce(<any>updateAccountDto);

      const updatedAccount = await accountService.updateConnectedAccount(<any>updateAccountDto);

      expect(updatedAccount).toEqual(updateAccountDto);
    });
  });

  describe('sendEmailToResetPassword', () => {
    it('should send email to reset password if email exists and account has password', async () => {
      // Arrange
      const forgotPasswordDto: ForgotPasswordDto = {
        email: 'jane.doe@hotmail.com',
      };
      const token = 'some-token';
      const accountFound = {
        email: forgotPasswordDto.email,
        password: 'some-password',
      };
      //   const emailData = {
      //     type: EmailType.RESET_PASSWORD,
      //     data: {
      //       redirectLink: `www.blabla-train.ci/user/reset-password?token=${token}`,
      //     },
      //     params: {
      //       to: [forgotPasswordDto.email],
      //     },
      //   };
      //   const mailServiceRunSpy = jest.spyOn(mailService, 'run');
      const tokenServiceGetTokenSpy = jest.spyOn(tokenService, 'getToken').mockReturnValue(token);
      const accountRepositoryFindByEmailSpy = jest
        .spyOn(accountRepository, 'findByEmail')
        .mockResolvedValue(<any>accountFound);

      // Act
      const result = await accountService.sendEmailToResetPassword(forgotPasswordDto);

      // Assert
      expect(result).toEqual(
        'Si nous trouvons un compte associé à cette adresse email alors un email de réinitialisation de mot de passe sera envoyé à ladite adresse',
      );
      expect(accountRepositoryFindByEmailSpy).toHaveBeenCalledWith(forgotPasswordDto.email);
      expect(tokenServiceGetTokenSpy).toHaveBeenCalledWith(TokenType.RESET_PASSWORD, {
        email: forgotPasswordDto.email,
      });
      //   expect(mailServiceRunSpy).toHaveBeenCalledWith(emailData);
    });

    it('should not send email to reset password if email does not exist', async () => {
      // Arrange
      const forgotPasswordDto: ForgotPasswordDto = {
        email: 'jane.doe@hotmail.com',
      };
      const accountFound = null;
      const accountRepositoryFindByEmailSpy = jest
        .spyOn(accountRepository, 'findByEmail')
        .mockResolvedValue(accountFound);
      const mailServiceRunSpy = jest.spyOn(mailService, 'run');

      // Act
      const result = await accountService.sendEmailToResetPassword(forgotPasswordDto);

      // Assert
      expect(result).toEqual(
        'Si nous trouvons un compte associé à cette adresse email alors un email de réinitialisation de mot de passe sera envoyé à ladite adresse',
      );
      expect(accountRepositoryFindByEmailSpy).toHaveBeenCalledWith(forgotPasswordDto.email);
      expect(mailServiceRunSpy).not.toHaveBeenCalled();
    });
  });

  describe('resetPassword', () => {
    it('should reset the password and return the updated account', async () => {
      // Arrange
      const resetPasswordDto: ResetPasswordDto = {
        password: 'newPassword',
      };
      const tokenServiceValidateTokenSpy = jest
        .spyOn(tokenService, 'validateToken')
        .mockReturnValue({ email: 'jane.doe@hotmail.com' });
      const accountFound = {
        id: 1,
        email: 'jane.doe@hotmail.com',
        password: 'oldPassword',
        salt: 'salt',
      };
      const bcryptHashSpy = jest.spyOn(bcrypt, 'hash').mockResolvedValue('newHashedPassword');
      const accountRepositoryFindByEmailSpy = jest
        .spyOn(accountRepository, 'findByEmail')
        .mockResolvedValue(<any>accountFound);
      const accountRepositoryUpdateAccountSpy = jest
        .spyOn(accountRepository, 'updateAccount')
        .mockResolvedValue(<any>{ ...accountFound, password: 'newHashedPassword' });

      // Act
      const result = await accountService.resetPassword(resetPasswordDto);

      // Assert
      expect(result).toEqual({ ...accountFound, password: 'newHashedPassword' });
      expect(tokenServiceValidateTokenSpy).toHaveBeenCalledWith(
        TokenType.RESET_PASSWORD,
        'Session expirée. Veuillez vous reconnecter',
      );
      expect(accountRepositoryFindByEmailSpy).toHaveBeenCalledWith(accountFound.email);
      expect(bcryptHashSpy).toHaveBeenCalledWith(resetPasswordDto.password, accountFound.salt);
      expect(accountRepositoryUpdateAccountSpy).toHaveBeenCalledWith(accountFound.id, {
        ...accountFound,
        password: 'newHashedPassword',
      });
    });

    it('should throw an error if the reset password token is invalid', async () => {
      // Arrange
      const resetPasswordDto: ResetPasswordDto = {
        password: 'newPassword',
      };
      const tokenServiceValidateTokenSpy = jest.spyOn(tokenService, 'validateToken').mockImplementation(() => {
        throw new Error('Invalid token');
      });

      // Act and Assert
      await expect(accountService.resetPassword(resetPasswordDto)).rejects.toThrow('Invalid token');
      expect(tokenServiceValidateTokenSpy).toHaveBeenCalledWith(
        TokenType.RESET_PASSWORD,
        'Session expirée. Veuillez vous reconnecter',
      );
    });
  });

  describe('changePassword', () => {
    it('should change password successfully', async () => {
      // Arrange
      const changePasswordDto: ChangePasswordDto = {
        oldPassword: 'oldPassword123',
        newPassword: 'newPassword123',
      };
      const accountFound = {
        id: REQUEST_META.CONNECTED_USER.id,
        password: 'hashed-oldPassword123',
        salt: 'some-salt',
      };
      const bcryptHashSpy = jest
        .spyOn(bcrypt, 'hash')
        .mockImplementation((password: string) => Promise.resolve(`hashed-${password}`));
      const accountRepositoryFindByIdSpy = jest
        .spyOn(accountRepository, 'findById')
        .mockResolvedValue(<any>accountFound);
      const accountRepositoryUpdateAccountSpy = jest
        .spyOn(accountRepository, 'updateAccount')
        .mockResolvedValue(<any>accountFound);

      // Act
      const result = await accountService.changePassword(changePasswordDto);

      // Assert
      expect(result).toEqual(accountFound);
      expect(accountRepositoryFindByIdSpy).toHaveBeenCalledWith(REQUEST_META.CONNECTED_USER.id);
      expect(accountRepositoryUpdateAccountSpy).toHaveBeenCalledWith(
        REQUEST_META.CONNECTED_USER.id,
        expect.objectContaining({
          password: 'hashed-newPassword123',
        }),
      );
      expect(bcryptHashSpy).toHaveBeenCalledWith(changePasswordDto.newPassword, accountFound.salt);
      expect(bcryptHashSpy).toHaveBeenCalledWith(changePasswordDto.oldPassword, accountFound.salt);
    });
  });
});
