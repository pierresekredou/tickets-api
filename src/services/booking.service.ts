import { BadRequestException, HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { BookingDto } from '../dto/booking.dto';
import { Booking } from '../utils/mongoose/interfaces/booking.interface';
import { AccountRepository, BookingRepository, TravelRepository } from '../repositories';
import { REQUEST_META } from '../utils/constants';
import { BookingStatus } from '../utils/enums/booking-status.enum';
import { EmailType } from '../mail/email-metadata/email-types.enum';
import { MailService } from '../mail/mail.service';
import { Travel } from '../utils/mongoose/interfaces/travel.interface';
import * as dayjs from 'dayjs';

@Injectable()
export class BookingService {
  constructor(
    private readonly bookingRepository: BookingRepository,
    private readonly travelRepository: TravelRepository,
    private readonly accountRepository: AccountRepository,
    private readonly mailService: MailService,
  ) {}

  async createBooking(bookingDto: BookingDto) {
    const userAccountId = REQUEST_META.CONNECTED_USER.id;
    const relatedTravel = await this.travelRepository.findById(bookingDto.travelId);
    if (!relatedTravel) throw new HttpException('Specified travel was not found!', HttpStatus.NOT_FOUND);
    const numberOfRemainingPlaces = relatedTravel.numberOfPlacesLeft - bookingDto.numberOfPlaces;
    if (numberOfRemainingPlaces < 0)
      throw new HttpException(
        `Not enough places left for this travel. You can book up to ${relatedTravel.numberOfPlacesLeft} places right now`,
        HttpStatus.NOT_ACCEPTABLE,
      );
    const bookingData = { ...bookingDto, userAccountId, code: this.generateBookingCode() };
    const booking = await this.bookingRepository.saveBooking(bookingData)?.catch((err) => {
      throw new BadRequestException(err.message);
    });
    relatedTravel.numberOfPlacesLeft = numberOfRemainingPlaces;
    await this.travelRepository.updateTravel(relatedTravel.id, relatedTravel)?.catch((err) => {
      throw new BadRequestException(err.message);
    });
    this.sendBookingCreatedEmail(booking, relatedTravel);
    return booking;
  }

  generateBookingCode() {
    return String(Date.now());
  }

  async sendBookingCreatedEmail(booking: Booking, relatedTravel: Travel) {
    const userAccount = await this.accountRepository.findById(REQUEST_META.CONNECTED_USER.id);
    const emailData = {
      type: EmailType.BOOKING_CREATED,
      data: {
        bookingCode: booking.code,
        departure: relatedTravel.departure,
        destination: relatedTravel.destination,
        travelDate: dayjs(relatedTravel.dateTime).format('DD-MM-YYYY'),
        userFullName: `${userAccount.firstname} ${userAccount.lastname}`,
      },
      params: {
        to: [userAccount.email],
      },
    };
    await this.mailService.run(emailData);
  }

  async getMyBookings(): Promise<Booking[]> {
    const bookings = await this.bookingRepository
      .findAllBookingsOfUser(REQUEST_META.CONNECTED_USER.id)
      ?.catch((err) => {
        throw new BadRequestException(err.message);
      });
    return bookings;
  }

  async getOneBooking(bookingId: string): Promise<Booking> {
    const booking = await this.bookingRepository
      .findOneBookingOfUser(bookingId, REQUEST_META.CONNECTED_USER.id)
      ?.catch((err) => {
        throw new BadRequestException(err.message);
      });
    return booking;
  }

  async updateBooking(bookingId: string, bookingDto: BookingDto): Promise<Booking> {
    const userAccountId = REQUEST_META.CONNECTED_USER.id;
    const bookingFound = await this.bookingRepository.findOneBookingOfUser(bookingId, userAccountId)?.catch((err) => {
      throw new BadRequestException(err.message);
    });
    if (!bookingFound) throw new HttpException(`Specified item was not found!`, HttpStatus.NOT_FOUND);
    if (bookingFound.status !== BookingStatus.WAITING)
      throw new HttpException(
        'Action not allowed at this time. The status of this booking does not allow any change',
        HttpStatus.NOT_FOUND,
      );
    const relatedTravel = await this.travelRepository.findById(bookingDto.travelId);
    if (!relatedTravel) throw new HttpException('Specified travel was not found!', HttpStatus.NOT_FOUND);
    const numberOfRemainingPlaces =
      relatedTravel.numberOfPlacesLeft + bookingFound.numberOfPlaces - bookingDto.numberOfPlaces;
    if (numberOfRemainingPlaces < 0)
      throw new HttpException(
        `Not enough places left for this travel. You can book up to ${
          relatedTravel.numberOfPlacesLeft + bookingFound.numberOfPlaces
        } places right now`,
        HttpStatus.NOT_ACCEPTABLE,
      );
    const bookingData = { ...bookingDto, userAccountId };
    const bookingUpdated = await this.bookingRepository.updateBooking(bookingId, bookingData)?.catch((err) => {
      throw new BadRequestException(err.message);
    });
    relatedTravel.numberOfPlacesLeft = numberOfRemainingPlaces;
    await this.travelRepository.updateTravel(relatedTravel.id, relatedTravel)?.catch((err) => {
      throw new BadRequestException(err.message);
    });

    return bookingUpdated;
  }

  async cancelBooking(bookingId: string): Promise<Booking> {
    const userAccountId = REQUEST_META.CONNECTED_USER.id;
    const bookingFound = await this.bookingRepository.findOneBookingOfUser(bookingId, userAccountId)?.catch((err) => {
      throw new BadRequestException(err.message);
    });
    if (!bookingFound) throw new HttpException(`Specified item was not found!`, HttpStatus.NOT_FOUND);
    if (bookingFound.status !== BookingStatus.WAITING)
      throw new HttpException(
        'Acrtion not allowed at this time. The status of this booking does not allow any change',
        HttpStatus.NOT_FOUND,
      );
    const relatedTravel = await this.travelRepository.findById(bookingFound.travelId);
    bookingFound.status = BookingStatus.CANCELED;
    const bookingUpdated = await this.bookingRepository.updateBooking(bookingId, bookingFound)?.catch((err) => {
      throw new BadRequestException(err.message);
    });
    relatedTravel.numberOfPlacesLeft += bookingFound.numberOfPlaces;
    await this.travelRepository.updateTravel(relatedTravel.id, relatedTravel)?.catch((err) => {
      throw new BadRequestException(err.message);
    });
    this.sendBookingCanceledEmail(bookingFound);
    return bookingUpdated;
  }

  async sendBookingCanceledEmail(booking: Booking) {
    const userAccount = await this.accountRepository.findById(REQUEST_META.CONNECTED_USER.id);
    const emailData = {
      type: EmailType.BOOKING_CANCELED,
      data: {
        bookingCode: booking.code,
        userFullName: `${userAccount.firstname} ${userAccount.lastname}`,
      },
      params: {
        to: [userAccount.email],
      },
    };
    await this.mailService.run(emailData);
  }
}
