import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { SignupDto } from '../dto/signup.dto';
import { AccountRepository } from '../repositories/account.repository';
import * as bcrypt from 'bcrypt';
import { SigninDto } from '../dto/signin.dto';
import { TokenService } from './token.service';
import { AccountStatus } from '../utils/enums/account-status.enum';
import { TokenType } from '../utils/enums/token-type.enum';
import { Account } from '../utils/mongoose/interfaces/account.interface';

@Injectable()
export class AuthService {
  constructor(private readonly accountRepository: AccountRepository, private readonly tokenService: TokenService) {}

  async signIn(signinDto: SigninDto): Promise<{ userAccount: Partial<Account>; accessToken: string }> {
    const { email, password } = signinDto;
    const account = await this.accountRepository.findByEmail(email);
    if (!account || !(await this.validatePassword(account, password))) {
      throw new HttpException('Invalid email or password', HttpStatus.FORBIDDEN);
    }
    if (account.status !== AccountStatus.ENABLED) {
      throw new HttpException(
        'Access denied. If you just signed up please check out your email to activate your account',
        HttpStatus.FORBIDDEN,
      );
    }
    const token = this.tokenService.getToken(TokenType.AUTH, { email: account.email });
    return {
      userAccount: account,
      accessToken: token,
    };
  }

  public async signUp(signupDto: SignupDto) {
    const userExists = await this.accountRepository.findByEmail(signupDto.email);
    if (userExists) {
      throw new HttpException(
        'An account with specified email address already exists. Please sign in with that email or choose another email address',
        HttpStatus.CONFLICT,
      );
    }
    let signupData: Partial<Account> = { ...signupDto, status: AccountStatus.ENABLED };
    const salt = await bcrypt.genSalt(15);
    const password = await bcrypt.hash(signupData.password, salt);
    signupData = { ...signupData, password, salt };
    const account = await this.accountRepository.saveAccount(signupData);
    return {
      userAccount: account,
    };
  }

  async validatePassword(account: Account, password: string): Promise<boolean> {
    const hash = await bcrypt.hash(password, account.salt);
    return hash === account?.password;
  }
}
