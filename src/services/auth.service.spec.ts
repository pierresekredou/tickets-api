import { AccountRepository } from '../repositories';
import { AccountStatus } from '../utils/enums/account-status.enum';
import { AuthService } from './auth.service';
import { TokenService } from './token.service';
import { REQUEST_META } from '../utils/constants';
import { connectedUserStub } from '../__mock__/connected-user-stub';
import { JwtService } from '@nestjs/jwt';
import { ACCOUNT_MODEL } from '../utils/mongoose/mongoose.constant';
import { Test, TestingModule } from '@nestjs/testing';
import { HttpException, HttpStatus } from '@nestjs/common';
import { Account } from '../utils/mongoose/interfaces/account.interface';
import { signupDto } from '../__mock__/auth-stub';
import { getModelToken } from '@nestjs/mongoose';

describe('AuthService', () => {
  let authService: AuthService;
  let accountRepository: AccountRepository;
  // let tokenService: TokenService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        AuthService,
        AccountRepository,
        TokenService,
        JwtService,
        {
          provide: ACCOUNT_MODEL,
          useValue: getModelToken('Account'),
        },
      ],
    }).compile();

    authService = module.get<AuthService>(AuthService);
    accountRepository = module.get<AccountRepository>(AccountRepository);
    // tokenService = module.get<TokenService>(TokenService);
    REQUEST_META.CONNECTED_USER = <any>connectedUserStub;
  });

  describe('signIn', () => {
    const account = {
      id: '1',
      email: 'test@test.com',
      password: '$2a$15$6CmVbK1qnFmFjK24CejH/u1a/SwZhn0KQ.SzU/Zq.Z4QXT6jKfp6W', // "testpassword"
      salt: '$2a$15$6CmVbK1qnFmFjK24CejH/u',
      status: AccountStatus.ENABLED,
    };
    const signinDto = {
      email: 'test@test.com',
      password: 'testpassword',
    };

    it('should throw an error if the email is invalid', async () => {
      jest.spyOn(accountRepository, 'findByEmail').mockResolvedValueOnce(null);
      await expect(authService.signIn(signinDto)).rejects.toThrow('Invalid email or password');
    });

    it('should throw an error if the password is invalid', async () => {
      jest.spyOn(accountRepository, 'findByEmail').mockResolvedValueOnce(<any>account);
      jest.spyOn(authService, 'validatePassword').mockResolvedValueOnce(false);
      await expect(authService.signIn(signinDto)).rejects.toThrow('Invalid email or password');
    });

    it('should return an access token if the email and password are valid', async () => {
      jest.spyOn(accountRepository, 'findByEmail').mockResolvedValueOnce(<any>account);
      jest.spyOn(authService, 'validatePassword').mockResolvedValueOnce(true);
      const result = await authService.signIn(signinDto);
      expect(result).toHaveProperty('accessToken');
      expect(result.userAccount).toEqual(account);
    });
  });

  describe('signUp', () => {
    it('should create a new account and return it', async () => {
      const account: Partial<Account> = {
        id: '1',
        firstname: 'John',
        lastname: 'Doe',
        email: 'mrp01@hotmail.com',
        password: 'password',
        salt: 'salt',
        status: AccountStatus.ENABLED,
      };
      jest.spyOn(accountRepository, 'findByEmail').mockResolvedValueOnce(null);
      jest.spyOn(accountRepository, 'saveAccount').mockResolvedValueOnce(<any>account);

      const result = await authService.signUp(signupDto);

      expect(result).toEqual({ userAccount: account });
      expect(accountRepository.findByEmail).toHaveBeenCalledWith(signupDto.email);
      expect(accountRepository.saveAccount).toHaveBeenCalledWith({
        ...signupDto,
        status: AccountStatus.ENABLED,
        salt: expect.any(String),
        password: expect.any(String),
      });
    });

    it('should throw an HttpException with status 409 if an account already exists with the provided email', async () => {
      // const signupDto: SignupDto = {
      //   firstname: 'John',
      //   lastname: 'Doe',
      //   email: 'john.doe@example.com',
      //   password: 'password',
      // };
      const existingAccount: Partial<Account> = {
        id: '1',
        firstname: 'John',
        lastname: 'Doe',
        email: 'mrp01@hotmail.com',
        password: 'password',
        salt: 'salt',
        status: AccountStatus.ENABLED,
      };
      jest.spyOn(accountRepository, 'findByEmail').mockResolvedValueOnce(<any>existingAccount);
      jest.spyOn(accountRepository, 'saveAccount');

      await expect(authService.signUp(signupDto)).rejects.toThrow(
        new HttpException(
          'An account with specified email address already exists. Please sign in with that email or choose another email address',
          HttpStatus.CONFLICT,
        ),
      );
      expect(accountRepository.findByEmail).toHaveBeenCalledWith(signupDto.email);
      expect(accountRepository.saveAccount).not.toHaveBeenCalled();
    });
  });
});
