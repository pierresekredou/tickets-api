import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { ForgotPasswordDto } from '../dto/forgot-password.dto';
import { ResetPasswordDto } from '../dto/reset-password.dto';
import * as bcrypt from 'bcrypt';
import { ChangePasswordDto } from '../dto/change-password.dto';
import { UpdateAccountDto } from '../dto/account.dto';
import { AccountRepository } from '../repositories';
import { REQUEST_META } from '../utils/constants';
import { TokenType } from '../utils/enums/token-type.enum';
import { MailService } from '../mail/mail.service';
import { EmailType } from '../mail/email-metadata/email-types.enum';
import { TokenService } from './token.service';

@Injectable()
export class AccountService {
  constructor(
    private readonly accountRepository: AccountRepository,
    private readonly mailService: MailService,
    private readonly tokenService: TokenService,
  ) {}

  async getConnectedAccount() {
    return REQUEST_META.CONNECTED_USER;
  }

  async updateConnectedAccount(updateAccountDto: UpdateAccountDto) {
    const id = REQUEST_META.CONNECTED_USER.id;
    const accountFound = await this.accountRepository.findById(id);
    if (!accountFound) {
      throw new HttpException('Specified account was not found', HttpStatus.CONFLICT);
    }
    const accountWithGivenEmailExists = await this.accountRepository.findByEmail(updateAccountDto.email);
    if (accountWithGivenEmailExists && accountWithGivenEmailExists.id != id) {
      throw new HttpException(
        'An account with specified email address already exists. Please choose another one',
        HttpStatus.CONFLICT,
      );
    }
    const account = await this.accountRepository.updateAccount(id, updateAccountDto);
    return account;
  }

  async sendEmailToResetPassword(forgotPasswordDto: ForgotPasswordDto) {
    const accountFound = await this.accountRepository.findByEmail(forgotPasswordDto.email);
    if (accountFound && accountFound.password) {
      const token = this.tokenService.getToken(TokenType.RESET_PASSWORD, { email: forgotPasswordDto.email });
      const emailData = {
        type: EmailType.RESET_PASSWORD,
        data: {
          redirectLink: `www.blabla-train.ci/user/reset-password?token=${token}`,
        },
        params: {
          to: [forgotPasswordDto.email],
        },
      };
      await this.mailService.run(emailData);
    }
    return 'Si nous trouvons un compte associé à cette adresse email alors un email de réinitialisation de mot de passe sera envoyé à ladite adresse';
  }

  async resetPassword(resetPasswordDto: ResetPasswordDto) {
    const { email } = this.tokenService.validateToken(
      TokenType.RESET_PASSWORD,
      'Session expirée. Veuillez vous reconnecter',
    );
    const accountFound = await this.accountRepository.findByEmail(email);
    accountFound.password = await bcrypt.hash(resetPasswordDto.password, accountFound.salt);
    const account = await this.accountRepository.updateAccount(accountFound.id, accountFound);
    return account;
  }

  async changePassword(changePasswordDto: ChangePasswordDto) {
    const accountId = REQUEST_META.CONNECTED_USER?.id;
    const accountFound = await this.accountRepository.findById(accountId);
    if (!accountFound) throw new HttpException('Specified item was not found!', HttpStatus.NOT_FOUND);
    const providedOldPasswordHashed = await bcrypt.hash(changePasswordDto.oldPassword, accountFound.salt);
    if (providedOldPasswordHashed !== accountFound.password) {
      throw new HttpException('Wrong password was given', HttpStatus.NOT_ACCEPTABLE);
    }
    accountFound.password = await bcrypt.hash(changePasswordDto.newPassword, accountFound.salt);
    const account = await this.accountRepository.updateAccount(accountFound.id, accountFound);
    return account;
  }
}
