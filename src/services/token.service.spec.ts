import { INestApplication } from '@nestjs/common';
import { Test, TestingModule } from '@nestjs/testing';
import { JwtService } from '@nestjs/jwt';
import { TokenService } from './token.service';
import { signinDto } from '../__mock__/auth-stub';
import { TokenType } from '../utils/enums/token-type.enum';

describe('TokenService', () => {
  let app: INestApplication;
  let testingModule: TestingModule;
  let JwtService: JwtService;
  let service: TokenService;

  const mockJwtService = {
    sign: jest.fn(),
    decode: jest.fn(),
    verify: jest.fn(),
  };

  const mockTokenService = {
    getToken: jest.fn(),
    validateToken: jest.fn(),
  };

  beforeEach(async () => {
    testingModule = await Test.createTestingModule({
      providers: [
        TokenService,
        {
          provide: TokenService,
          useValue: mockTokenService,
        },
      ],
    })
      .overrideProvider(JwtService)
      .useValue(mockJwtService)
      .compile();
    app = testingModule.createNestApplication();
    service = app.get<TokenService>(TokenService);
    await app.init();
  });

  it('TokenService - should be defined', () => {
    expect(service).toBeDefined();
  });

  describe('getToken', () => {
    it('should generate and return a new Jwt token', async () => {
      const getTokenSpy = jest.spyOn(service, 'getToken');

      const email = signinDto.email;
      expect(service.getToken(TokenType.AUTH, { email })).toBe(undefined);
      expect(getTokenSpy).toHaveBeenCalledWith(TokenType.AUTH, { email });
    });
  });

  describe('validateToken', () => {
    it('should check a given Jwt token validity', async () => {
      const validateTokenSpy = jest.spyOn(service, 'validateToken');

      expect(service.validateToken(TokenType.AUTH)).toBe(undefined);
      expect(validateTokenSpy).toHaveBeenCalledWith(TokenType.AUTH);
    });
  });
});
