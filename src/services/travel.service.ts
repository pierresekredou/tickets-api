import { BadRequestException, Injectable } from '@nestjs/common';
import { TravelDto } from '../dto/travel.dto';
import { Travel } from '../utils/mongoose/interfaces/travel.interface';
import { TravelRepository } from '../repositories';

@Injectable()
export class TravelService {
  constructor(private readonly travelRepository: TravelRepository) {}

  async createTravel(travelDto: TravelDto) {
    const travelData = {
      ...travelDto,
      numberOfPlacesLeft: travelDto.totalPlaces,
    };
    return await this.travelRepository.saveTravel(travelData)?.catch((err) => {
      throw new BadRequestException(err.message);
    });
  }

  async getAvailableTravels(): Promise<Travel[]> {
    const travels = await this.travelRepository.findAvailableTravelsForBooking()?.catch((err) => {
      throw new BadRequestException(err.message);
    });
    return travels;
  }

  async getOneTravel(travelId: string): Promise<Travel> {
    const travel = await this.travelRepository.findById(travelId)?.catch((err) => {
      throw new BadRequestException(err.message);
    });
    return travel;
  }

  async updateTravel(travelId: string, travelDto: TravelDto): Promise<Travel> {
    const travelData = {
      ...travelDto,
      numberOfPlacesLeft: travelDto.totalPlaces,
    };
    const travel = await this.travelRepository.updateTravel(travelId, travelData)?.catch((err) => {
      throw new BadRequestException(err.message);
    });
    return travel;
  }

  async deleteTravel(travelId: string): Promise<string> {
    await this.travelRepository.removeTravel(travelId)?.catch((err) => {
      throw new BadRequestException(err.message);
    });
    return 'OK';
  }

  async searchTravels(departure: string, destination: string, dateTime: string) {
    return await this.travelRepository.searchTravels(departure, destination, dateTime)?.catch((err) => {
      throw new BadRequestException(err.message);
    });
  }
}
